{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ../../pkgs/base-packages.nix
      ../../pkgs/desktop-packages.nix
      ../../modules/emacs.nix
      ../../modules/desktop.nix
      ../../modules/base.nix
      ../../modules/gaming.nix
    ];

  networking.hostName = "kaladin"; # Define your hostname.

  networking.interfaces.enp0s31f6.useDHCP = true;
  networking.interfaces.wlp7s0.useDHCP = true;

  # NVIDIA
  # services.xserver.videoDrivers = [ "nvidia" ];
  hardware = {
    opengl = {
      enable = true;
      driSupport32Bit = true;
      extraPackages = with pkgs; [
        mesa.drivers
        vaapiVdpau
        libvdpau-va-gl
      ];
    };
    # nvidia = {
    #   package = config.boot.kernelPackages.nvidiaPackages.beta;
    #   modesetting.enable = true;
    #   powerManagement.enable = true;
    # };
  };
  services.xserver.videoDrivers = [ "amdgpu" ];

  # services.xserver.screenSection = ''
  # Option         "metamodes" "nvidia-auto-select +0+0 {ForceFullCompositionPipeline=On}"
  # Option         "AllowIndirectGLXProtocol" "off"
  # Option         "TripleBuffer" "on"
  # '';

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.chris = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "input" "no-internet" ]; # Enable ‘sudo’ for the user.
  };

  # virtualisation.waydroid.enable = true;

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  
  # services.caddy = {
  #   enable = true;
  #   virtualHosts = {
  #     "videosdani.tfcconnection.org".extraConfig = ''
  #     reverse_proxy localhost:9000
  #     '';
  #   };
  #   virtualHosts = {
  #     "videosdani.tfcconnection.org:1935".extraConfig = ''
  #     reverse_proxy localhost:1935
  #     '';
  #   };
  # };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?

}
