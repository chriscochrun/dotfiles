{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "nvme" "usbhid" "usb_storage" "sd_mod" "sr_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/af62c65b-943d-4a8a-8f77-ad7826729aba";
      fsType = "btrfs";
      options = [ "subvol=@" "noatime" "ssd" "space_cache" "clear_cache" "compress=zstd" ];
    };

  fileSystems."/home" =
    { device = "/dev/disk/by-uuid/af62c65b-943d-4a8a-8f77-ad7826729aba";
      fsType = "btrfs";
      options = [ "subvol=@home" "noatime" "ssd" "space_cache" "clear_cache" "compress=zstd" ];
    };

  fileSystems."/snapshots" =
    { device = "/dev/disk/by-uuid/af62c65b-943d-4a8a-8f77-ad7826729aba";
      fsType = "btrfs";
      options = [ "subvol=snapshots" "noatime" "ssd" "space_cache" "clear_cache" "compress=zstd" ];
    };


 #fileSystems."/nix" =
 #  { device = "/dev/disk/by-uuid/9b5a1a62-0de6-4e07-a541-634736980d10";
 #    fsType = "btrfs";
 #    options = [ "subvol=@nix" "noatime" "ssd" "space_cache" "clear_cache" "compress=zstd" ];
 #  };

 #fileSystems."/var/log" =
 #  { device = "/dev/disk/by-uuid/9b5a1a62-0de6-4e07-a541-634736980d10";
 #    fsType = "btrfs";
 #    options = [ "subvol=@log" "noatime" "ssd" "space_cache" "clear_cache" "compress=zstd" ];
 #  };

  fileSystems."/run/media/chris/Storage" =
    { device = "/dev/disk/by-uuid/4c7d4273-7b72-4aa8-8e1c-e281543d06cb";
      fsType = "btrfs";
      options = [ "noatime" "space_cache" "clear_cache" "compress=zstd" ];
    };

  fileSystems."/run/media/chris/backup" =
    { device = "/dev/disk/by-uuid/4286b9ef-e8ed-49a0-9eec-91b8ee05b2cb";
      fsType = "ext4";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/35A0-C1F1";
      fsType = "vfat";
      options = [ "fmask=0022" "dmask=0022" ];
    };

  swapDevices = [ ];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.enp0s31f6.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlp7s0.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
