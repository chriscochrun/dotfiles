{ lib, config, pkgs, callPackage, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ../../cachix/default.nix
      ../../pkgs/base-packages.nix
      ../../pkgs/desktop-packages.nix
      ../../modules/base.nix
      ../../modules/emacs.nix
      ../../modules/desktop.nix
    ];
  networking.hostName = "syl"; # Define your hostname.

  boot.initrd.kernelModules = [ "i915" ];
  hardware.cpu.intel.updateMicrocode =
    lib.mkDefault config.hardware.enableRedistributableFirmware;

  hardware.acpilight.enable = lib.mkDefault true;
  networking.interfaces.wlp170s0.useDHCP = true;

  hardware.uinput.enable = true;
  hardware.bluetooth.enable = true;

  # nixpkgs.config.packageOverrides = pkgs: {
  #   vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  # };

  services.xserver.videoDrivers = ["intel"];
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
      libvdpau-va-gl
    ];
  };

  systemd.services.display-manager = {
    wants = [ "systemd-user-sessions.service" "multi-user.target" "network-online.target" ];
    after = [ "systemd-user-sessions.service" "multi-user.target" "network-online.target" ];
  };

  # Enable fingerprint
  services.fprintd.enable = true;

  # Power settings
  services.thermald.enable = true;
  services.power-profiles-daemon.enable = false;
  services.tlp = {
    enable = true;
    settings = {
      CPU_SCALING_GOVERNOR_ON_AC="performance";
      CPU_SCALING_GOVERNOR_ON_BAT="powersave";
      CPU_ENERGY_PERF_POLICY_ON_AC="balance_performance";
      CPU_ENERGY_PERF_POLICY_ON_BAT="power";
      PCIE_ASPM_ON_AC="default";
      PCIE_ASPM_ON_BAT="powersupersave";
      SCHED_POWERSAVE_ON_AC=0;
      SCHED_POWERSAVE_ON_BAT=1;
      START_CHARGE_THRESH_BAT1=70;
      STOP_CHARGE_THRESH_BAT1=80;
      USB_ALLOWLIST="32ac:0002";
      RUNTIME_PM_ON_AC="on";
      RUNTIME_PM_ON_BAT="auto";
    };
  };

#   SUBSYSTEM=="power_supply", ATTR{online}=="0", RUN+="/usr/bin/at -M -f /lib/udev/power-profiles/power-saver now"
# SUBSYSTEM=="power_supply", ATTR{online}=="1", RUN+="/usr/bin/at -M -f /lib/udev/power-profiles/performance now"

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;
  services.xserver.dpi = 144;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.chris = {
    isNormalUser = true;
    extraGroups = [ "adbusers" "wheel" "networkmanager" "input" "uinput" ];
  };

  services.fwupd.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  services.locate = {
    enable = true;
    package = pkgs.plocate;
    localuser = null;
    interval = "hourly";
    prunePaths = [ "/tmp" "/nix/store" "/nix/var/log/nix" "/etc" "/bin" "/boot" "/usr" "/var" "/run" "/srv" "/sys" "/root" "/proc" "/opt" "/dev" "/home/chris/.emacs.d" "/home/chris/.config" "/home/chris/.nix-profile" "/home/chris/.profile" "/home/chris/dev/nixpkgs" "/home/chris/.cache" "/home/chris/.local" "/home/chris/.dotemacs" "/home/chris/.cargo" "/home/chris/.mozilla" "/home/chris/.var" "/home/chris/.dotfiles" "/home/chris/.dbus" "/home/chris/.android" "/var/lib"];
  };

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?

}
