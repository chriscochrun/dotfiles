{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ../../modules/base.nix
      ../../modules/servers.nix
      ../../pkgs/base-packages.nix
    ];

  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  networking.hostName = "kohlin"; # Define your hostname.

  hardware.opengl = {
   enable = true;
   extraPackages = with pkgs; [
    intel-media-driver
    vaapiIntel
    vaapiVdpau
    libvdpau-va-gl    
   ];
  };
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users = {
    chris = {
      isNormalUser = true;
      extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    };
  };

  virtualisation.docker.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  services.syncthing = {
    enable = true;
    dataDir = "/storage/syncthing";
    openDefaultPorts = true;
    configDir = "/home/chris/syncthing";
    user = "chris";
    group = "users";
    guiAddress = "0.0.0.0:8384";
    devices = {
      syl = {
        id = "AJMADOK-TENODAA-VSOEW2A-4RXY2XI-YNHIS7H-H3ZYAO5-3UQ64EE-O2N5BAY";
      };
      kaladin = {
        id = "LH6523Z-QQ5F3A4-SINZDOI-UFMQBIX-ZV6Q5BQ-LTKVMDB-CRI6QG5-RRKJFQS";
      };
      shadow = {
        id = "SGO2BUT-WDOB2B7-SD5BHKU-ES3BFZF-EZPFSQJ-B4744TP-SXKZS4O-SDLJ5QX";
      };
      tablet = {
        id = "4HEXCNH-MCVBZQX-LQ735TG-P2VTJ7N-CZ5MK4P-ICZAPC7-YCXVEWV-7NILMA5";
      };
      dalinar = {
        id = "I6BKBTH-EYKIX5P-GRN6WY5-JHZNFM2-PKMNYU7-Q6JQQY5-EBEWSHO-XGZINAR";
      };
    };
    folders = {
      music = {
        id = "teqqy-rzvec";
        path = "/storage/syncthing/Music";
        devices = [ "syl" "kaladin" "shadow" "dalinar"];
        versioning = {
          type = "trashcan";
          params.cleanoutDays = "100";
        };
      };
      ebooks = {
        id = "wziqy-7hyv9";
        path = "/storage/syncthing/ebooks";
        devices = [ "syl" "kaladin" "shadow" "tablet" "dalinar"];
        versioning = {
          type = "trashcan";
          params.cleanoutDays = "100";
        };
      };
      notes = {
        id = "zwshm-umwbt";
        path = "/storage/syncthing/notes";
        devices = [ "syl" "kaladin" "shadow" "dalinar"];
        versioning = {
          type = "trashcan";
          params.cleanoutDays = "100";
        };
      };
      librera = {
        id = "vvtvj-osndy";
        path = "/storage/syncthing/librera";
        devices = [ "tablet" "shadow" "dalinar"];
        versioning = {
          type = "trashcan";
          params.cleanoutDays = "100";
        };
      };
    };
  };

  # List services that you want to enable:

  nix-bitcoin = {
    generateSecrets = true;
    operator = {
      enable = true;
      name = "chris";
    };
  };

  services.locate = {
    enable = true;
    locate = pkgs.plocate;
    localuser = null;
  };

  services.openssh.enable = true;
  services.fstrim.enable = true;
  services.logind = {
    lidSwitch = "ignore";
    lidSwitchExternalPower = "ignore";
  };

  services.monero = {
    enable = false;
    dataDir = "/storage/monero";
  };

  services.bitcoind = {
    enable = true;
    listen = true;
    dataDir = "/storage/bitcoind";
    dbCache = 3000;
  };

  services.clightning = {
    enable = true;
    dataDir = "/storage/clightning";
  };

  services.rtl = {
    enable = true;
    dataDir = "/storage/rtl";
    nightTheme = true;
    extraCurrency = "USD";
    nodes.clightning.enable = true;
  };

  # CADDY
  services.caddy = {
    enable = true;
  };

  systemd.services = {
    # mail-cert-renew = {
    #   enable = true;
    #   serviceConfig = {
    #     Type = "oneshot";
    #     ExecStart = with pkgs.docker "sh -c /home/chris/bin/mail-cert-renew";
    #   };
    # };
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
