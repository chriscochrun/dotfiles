#!/bin/bash

ID=$1

while true
do
    COMMAND="systemmonitor"
    # font='VictorMono Nerd Font' color=#ff9f43 size=11

    read used total <<< $(free -m | awk '/Mem/{printf $2" "$3}')

    percent=$(bc -l <<< "100 * $total / $used")

    mem=$(awk -v u=$used -v t=$total -v p=$percent 'BEGIN {printf "%sMi/%sMi %.1f% ", t, u, p}'| awk '{printf $2}')

    DATA="| A |  $mem | |$COMMAND |"
    qdbus org.kde.plasma.doityourselfbar /id_$ID \
          org.kde.plasma.doityourselfbar.pass "$DATA"

    sleep 2s
done
