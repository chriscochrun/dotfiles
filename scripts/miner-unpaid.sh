#!/usr/bin/env sh
eth=$(curl -X GET "https://eth.2miners.com/api/accounts/bc1qarhl2mwgdnatdn3q8fjy794c7jtmhtvjvsztd4" -H  "accept: application/json" | jq '.stats.balance' | sed 's/^/0\.00/')
echo "current eth: $eth"

rate=$(curl rate.sx/eth | rg avg | awk '{print $2}' | sed 's/\$//')
echo $rate

dollar=$(echo "$eth * $rate" | bc -l)
echo $dollar
