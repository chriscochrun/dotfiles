#!/bin/sh

# Let's setup the streaming service for LuctorCRC
peertubestreamkey=$(kdialog --password "What is the stream key for the website?")
facebookstreamkey=$(kdialog --password "What is the stream key for Facebook?")
echo $peertubestreamkey
echo $facebookstreamkey

ssh chris@40.122.147.124 sudo sed -i -e "s#luctorcrc.org.*#luctorcrc.org/live/$peertubestreamkey\;#" nginx-restream/data/nginx.conf
ssh chris@40.122.147.124 sudo sed -i -e "s#40.122.147.124.*#40.122.147.124:1936/rtmp/$facebookstreamkey\;#" nginx-restream/data/nginx.conf
# ssh chris@40.122.147.124 'sudo systemctl restart nginx'
ssh chris@40.122.147.124 "cd /home/chris/nginx-restream && sudo docker-compose down && sudo docker-compose up -d"

if [ $? = 0 ]; then
    kdialog --title "Success!" --msgbox "All set! Now you can start the stream!"
else
    kdialog --error "Sorry there was an error! Find Chris!"
fi
