#!/usr/bin/env bash

waybar=$(pgrep waybar)

if [[ $waybar > 0 ]]; then
    kill $waybar;
    echo "killing $waybar"
else
    exec waybar
    echo "starting waybar"
fi
