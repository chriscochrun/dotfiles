#!/bin/sh
# awk '{u=$2+$4; t=$2+$4+$5; if (NR==1){u1=u; t1=t;} else print ($2+$4-u1) * 100 / (t-t1) "%"; }' \
    #          <(rg 'cpu ' /proc/stat) <(sleep 1;rg 'cpu ' /proc/stat)
# echo 100 - $(mpstat | rg all | cut -d \  -f43) | bc
cpu="$(mpstat 1 1 | rg Average | awk '{print $3+$4+$5}' | sed 's/\(.*\)/\1%/g')"

# echo " $cpu"
echo " $cpu | bash='alacritty -e btop --class btop' color=#D75F00 font='VictorMono Nerd Font' size=11 iconname=cpu-symbolic"
