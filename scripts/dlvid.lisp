#!/usr/bin/env -S sbcl --script
(require "uiop")
(load "/home/chris/.sbclrc")
(ql:quickload :serapeum)

(defvar *url* (car (uiop:command-line-arguments)))

(defun dlvid (url)
  (run-program "yt-dlp"
               `("-o"
                 "~/vids/%(title)s.%(ext)s"
                 ,url
                 "--embed-thumbnail"
                 "--sponsorblock-remove=sponsor,intro,outro")
                 :search "/run/current-system/sw/bin/"
                 :output t))

(defun main ()
  (if (serapeum:string-contains-p "inv.cochrun.xyz" *url*)
      (let ((url (serapeum:string-replace-all
                  "inv.cochrun.xyz" *url* "youtube.com")))
        (dlvid (string url)))
    (dlvid (string url))))

 (main)
