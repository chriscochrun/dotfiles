#!/usr/bin/env bash
# Get the percentage of used memory and print it
read used total <<< $(free -m | awk '/Mem/{printf $2" "$3}')

percent=$(bc -l <<< "100 * $total / $used")

mem=$(awk -v u=$used -v t=$total -v p=$percent 'BEGIN {printf "%sMi/%sMi %.1f% ", t, u, p}'| awk '{printf $2}')


# echo " $mem "
echo " $mem | bash='alacritty -e btop --class btop' color=#9aedfe font='VictorMono Nerd Font' size=11 iconname=memory-symbolic"
# echo " $mem | color=#007687 font='VictorMono Nerd Font' size=11"
