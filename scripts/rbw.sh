#!/usr/bin/env bash
# Very basic interface for rbw using rofi

if [ $(hostname) = "syl" ]; then
    style="laptop-rbw-wayland"
    #echo "this is hidpi"
else 
    style="desktop-rbw"
    #echo "this is not hidpi"
fi

# Get all password files and create an array
root=~/.password-store
CACHE=~/.local/tmp/pass_rofi
seat=seat0

rbw sync
rbw list

list_passwords() {
    rbw list
}

passwords=$(rbw list)

prompt='search for passwords...'
SECRET=$(list_passwords | rofi -sync -i -p="${prompt}" -dmenu -theme ~/.config/rofi/launchers-git/$style.rasi)

# Ask whether pass, user or both are required

options=("Password" \
             "User" \
             "User and password" \
             "QR-Code" \
	     "OTP")

option=$(printf '%s\n' "${options[@]%}" | rofi -sync -i -dmenu -width 400 -lines 4 -prompt="..." -theme ~/.config/rofi/launchers-git/$style.rasi)

echo $option

case ${option} in
    Password )
	echo "${SECRET}"
	wtype $(rbw get "${SECRET}")
	;;
    User )
	wtype $(rbw get --full "${SECRET}" | rg Username: | awk '{$1 = ""; print $0}')
	;;
    "User and password" )
	wtype $(rbw get --full "${SECRET}" | rg Username: | awk '{$1 = ""; print $0}')
	wtype -P tab
	wtype $(rbw get "${SECRET}")
	;;
    "QR-Code" )
	if [[ $SECRET =~ wifi$ ]]; then
	    # Produce a valid wifi QR-code
	    WIFISSID=$(pass get_user ${SECRET})
	    WIFIPASS=$(pass get_pass ${SECRET})
	    WIFIQR="WIFI:T:WPA;S:${WIFISSID};P:${WIFIPASS};;"
	    qrencode -s 8 -o - $WIFIQR | feh --title "pass: QR-WIFI" -
	else
	    # Only password
	    pass show -q1 ${SECRET}
	fi
	;;
    "OTP" )
	wtype $(rbw code "${SECRET}")
	;;
esac

# wl-copy -o -s ${seat} ${PASSWD_PASS}
