;; #!/run/current-system/sw/bin/sbcl --script

;; (load "~/quicklisp/setup.lisp")
;; (ql:quickload '(drakma lquery))

(defun get-images ()
  (loop for i from 748 to 1489 do
    (let* ((request (drakma:http-request
		     (concatenate 'string "https://www.lfg.co/page/" (write-to-string i))))
	   (parsed-content (lquery:$ (lquery:initialize request)))
	   (url (vector-pop (lquery:$ parsed-content "img" (attr :src))))
	   (img (drakma:http-request url)))

      (if (not (search ".jpg" url))
	  (with-open-file (stream
			   (concatenate 'string "/home/chris/Pictures/lfg/" (write-to-string i) ".gif")
			   :direction :output
			   :if-exists :supersede
			   :if-does-not-exist :create
			   :element-type 'unsigned-byte)
	    (write-sequence img stream))
	  (with-open-file (stream
			   (concatenate 'string "/home/chris/Pictures/lfg/" (write-to-string i) ".jpg")
			   :direction :output
			   :if-exists :supersede
			   :if-does-not-exist :create
			   :element-type 'unsigned-byte)
	    (write-sequence img stream))))))

(get-images)
