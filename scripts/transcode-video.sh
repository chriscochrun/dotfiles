#!/bin/sh

for filename in *.mov; do
    ffmpeg -hwaccel vaapi -hwaccel_output_format vaapi -hwaccel_device /dev/dri/renderD128 -i "${filename}" -vf 'format=nv12,hwupload' -c:v hevc_vaapi -c:a aac -crf 22 "${filename%.*}".mp4
    rm $filename
done
