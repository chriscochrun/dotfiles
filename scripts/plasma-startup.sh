#!/bin/sh

export MOZ_ENABLE_WAYLAND=1
export QT_SCALE_FACTOR=1

# exec ydotoold &
# exec systemctl enable --user --now libinput-gestures &
# exec emacs --daemon &


systemctl --user stop jellyfin-mpv-shim.service &
systemctl --user stop nextcloud-client.service &
systemctl --user stop emacs.service &

sleep 1

exec emacs --daemon &
exec jellyfin-mpv-shim &
exec nextcloud --background &
