#!/bin/sh
ping=$(ping -c 4 1.1.1.1 | tail -1| awk '{print $4}' | cut -d '/' -f 2)

echo " $ping | color=#57c7ff font='VictorMono Nerd Font' size=11"
