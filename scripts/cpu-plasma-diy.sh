#!/bin/bash

ID=$1

while true
do
    COMMAND="systemmonitor"
    # font='VictorMono Nerd Font' color=#ff9f43 size=11

    cpu="$(mpstat 1 1 | rg Average | awk '{print $3+$4+$5}' | sed 's/\(.*\)/\1%/g')"

    DATA="| A |  $cpu | | $COMMAND |"
    qdbus org.kde.plasma.doityourselfbar /id_$ID \
          org.kde.plasma.doityourselfbar.pass "$DATA"

    sleep 3s
done
