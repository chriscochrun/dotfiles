#!/usr/bin/emacs --script
;;; mediashout-to-openlp.el --- convert ms text to openlp text  -*- lexical-binding: t; -*-

;;;###autoload
(defun bar ()
  "Print foo bar."
  (interactive)
  (message "Foo Bar!"))

;;;; CLI

(defun running-as-script-p ()
  "Return truthy if running as Elisp script."
  (member "-scriptload" command-line-args))

(defun main ()
  "Entrypoint"
  (print command-line-args-left)

  (find-file (car command-line-args-left))
  (with-current-buffer
      (goto-char (point-min))
    (search-forward "Title: ")
    (delete-char 7 t)
    )


  (search-forward "Copyright: ")

  (message "Do stuff here"))

(when (running-as-script-p)
  (main))

(provide 'mediashout-to-openlp)
