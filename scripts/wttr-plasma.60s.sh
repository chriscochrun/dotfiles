#!/bin/sh
weather=$(curl 'wttr.in/long_island,ks?format=%c%t++%h++%p')

echo "$weather | color=#57c7ff font='VictorMono Nerd Font' size=11"
