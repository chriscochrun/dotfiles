#!/usr/bin/env bash

folders=( 0 1 2 3 4 5 6 )

# while [ 1 ];
# do
echo "" > /tmp/ncsync 
syncing=$(cat /tmp/ncsync)
for folder in ${folders[@]}
do
    name=$(qdbus com.nextcloudgmbh.Nextcloud /com/nextcloudgmbh/Nextcloud/Folder/$folder org.freedesktop.CloudProviders.Account.Name)
    fullstatus=$(qdbus com.nextcloudgmbh.Nextcloud /com/nextcloudgmbh/Nextcloud/Folder/$folder org.freedesktop.CloudProviders.Account.StatusDetails)
    status=$(echo $fullstatus | awk '{print $3}' | sed 's/,//')
    echo $name - $fullstatus >> /tmp/ncsync 
done
    # sleep 3
# done

