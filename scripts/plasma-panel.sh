#!/usr/bin/env bash

hiding=$(qdbus org.kde.plasmashell /PlasmaShell evaluateScript "p = panelById(panelIds[0]); print(p.hiding);")

if [ $hiding == "none" ]
then
    qdbus org.kde.plasmashell /PlasmaShell evaluateScript "p = panelById(panelIds[0]); p.hiding = 'autohide';"
else
    qdbus org.kde.plasmashell /PlasmaShell evaluateScript "p = panelById(panelIds[0]); p.hiding = 'none';"
fi

echo $hiding
