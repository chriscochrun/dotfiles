#!/bin/sh

systemsettings5 --list | rg - | awk '{print $1}' | rofi -dmenu -theme ~/.config/rofi/launchers-git/laptop-rbw-wayland.rasi | xargs -r systemsettings5
