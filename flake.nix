{
  description = "The Flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-23.11";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    home-manager = {
      url = "github:nix-community/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    stylix = {
      url = "github:danth/stylix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # hyprland = {
    #   type = "git";
    #   url = "https://github.com/hyprwm/Hyprland";
    #   submodules = true;
    #   inputs.nixpkgs.follows = "nixpkgs-stable";
    # };
    # hyprland-plugins = {
    #   url = "github:hyprwm/hyprland-plugins";
    #   inputs.hyprland.follows = "hyprland";
    # };
    radicle = {
      type = "git";
      url = "https://seed.radicle.garden/z3gqcJUoA1n9HaHKufZs5FCSGazv5.git";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    eww = {
      url = "github:elkowar/eww";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    ags = {
      url = "github:Aylur/ags";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    emacs = {
      url = "github:nix-community/emacs-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-bitcoin = {
      url = "github:fort-nix/nix-bitcoin/release";
    };
    simple-nixos-mailserver = {
      url = "gitlab:simple-nixos-mailserver/nixos-mailserver/nixos-22.11";
    };
    # anyrun = {
    #   url = "github:Kirottu/anyrun";
    #   inputs.nixpkgs.follows = "nixpkgs";
    # };
    libre-presenter = {
      url = "gitlab:chriscochrun/lumina";
    };
    # nixified-ai = {
    #   url = "github:nixified-ai/flake";
    #   inputs.nixpkgs.follows = "nixpkgs";
    # };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs,
              nixpkgs-stable,
              home-manager,
              # hyprland,
              nixos-hardware,
              emacs,
              nix-bitcoin,
              libre-presenter,
              eww,
              ags,
              stylix,
              radicle,
              rust-overlay, ... }:
    let
      system = "x86_64-linux";
      username = "chris";
      stable = import nixpkgs-stable {
        inherit system;
        config = { allowUnfree = true; };
      };
      pkgsForSystem = import nixpkgs {
        inherit system;
        config = { allowUnfree = true; };
        overlays = [
          emacs.overlays.default
          rust-overlay.overlays.default
          eww.overlays.default
        ];
      };

      lib = nixpkgs.lib;

    in  {
      nixosConfigurations = {
        syl = lib.nixosSystem {
          inherit system;
          pkgs = pkgsForSystem;
          specialArgs = {
            stable = stable;
          };
          modules = [
            ./systems/syl/configuration.nix
            home-manager.nixosModules.home-manager
            # hyprland.nixosModules.default
            # stylix.nixosModules.stylix
            nixos-hardware.nixosModules.framework-11th-gen-intel
            # libre-presenter.defaultPackage
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.extraSpecialArgs = { inherit ags; };
              home-manager.users.chris = import ./home/home.nix;
            }
          ];
        };
        kaladin = lib.nixosSystem {
          inherit system;
          pkgs = pkgsForSystem;
          modules = [
            ./systems/kaladin/configuration.nix
            home-manager.nixosModules.home-manager
            # hyprland.nixosModules.default
            # stylix.nixosModules.stylix
            nixos-hardware.nixosModules.common-gpu-amd
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.extraSpecialArgs = { inherit ags; };
              home-manager.users.chris = import ./home/home.nix;
            }
          ];
        };
        dalinar = nixpkgs-stable.lib.nixosSystem {
          inherit system;
          modules = [
            ./systems/dalinar/configuration.nix
            nix-bitcoin.nixosModules.default
            # simple-nixos-mailserver.nixosModules.default
          ];
        };
        kohlin = nixpkgs-stable.lib.nixosSystem {
          inherit system;
          modules = [
            ./systems/kohlin/configuration.nix
            nix-bitcoin.nixosModules.default
          ];
        };
      };
    };
}
