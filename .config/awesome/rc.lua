fennel = require("fennel")
local gears = require("gears")
local naughty = require("naughty")
cfgDir = os.getenv("HOME") .. "/.config/awesome/"
-- package.path = cfgDir .. "?.lua"
fennel.path = fennel.path .. ";" .. cfgDir .. "?.fnl;"


-- naughty.notification{ title = "YAY RC LOADED"}

searcher = fennel.make_searcher({
      correlate = true,
         useMetadata = true,
         -- disable strict checking.
         -- TODO: assemble a full list of globals so we can enable this
         -- allowedGlobals = false
   })

table.insert(package.loaders or package.searchers, fennel.searcher)
debug.traceback = fennel.traceback

-- require("lib")
require("init") -- load ~/.config/awesome/init.fnl

