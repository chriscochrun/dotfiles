---------------------------
-- Default awesome theme --
---------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local gears = require('gears')

local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()
local wallpaperdir = "/home/chris/Pictures/wallpapers/"
local bling = require("bling")
local configdir = gfs.get_configuration_dir()

local theme = {}

theme.font          = "VictorMono Nerd Font SemiBold 10"

theme.mstab_border_radius = 50              -- border radius of the tabbar
-- COLORS
-- Try to build a theme off of SNAZZY
theme.base00        = "#282a36"
theme.base01        = "#34353e"
theme.base02        = "#43454f"
theme.base03        = "#78787e"
theme.base04        = "#a5a5a9"
theme.base05        = "#e2e4e5"
theme.base06        = "#eff0eb"
theme.base07        = "#f1f1f0"
theme.base08        = "#ff5c57"
theme.base09        = "#ff9f43"
theme.base0A        = "#f3f99d"
theme.base0B        = "#5af78e"
theme.base0C        = "#9aedfe"
theme.base0D        = "#57c7ff"
theme.base0E        = "#ff6ac1"
theme.base0F        = "#b2643c"

theme.bg_darker     = "#1a1a25"

theme.bg_normal     = theme.base00
theme.bg_opac20     = "#282a36CD"
theme.bg_light      = theme.base01
theme.bg_focus      = theme.base0D -- Base02
theme.bg_minimize   = theme.base02
theme.bg_urgent     = "#ff5c57"

theme.fg_normal     = theme.base05
theme.fg_focus      = theme.base03
theme.fg_urgent     = "#e2e4e5"
theme.fg_minimize   = theme.base04

theme.red           = "#ff5c57"

theme.useless_gap   = dpi(9)
theme.border_width  = dpi(0)
theme.transparent   = "#00000000"
theme.bg_systray    = "#282a36AA"
theme.border_normal = "#00000000"
theme.border_focus  = "#00000000"
theme.border_marked = "#00000000"


-- theme.mstab_tabbar_height = 400
theme.tabbar_bg_normal = theme.bg_normal
theme.tabbar_fg_normal = theme.fg_normal
theme.tabbar_bg_focus = theme.bg_focus
theme.tabbar_fg_focus = theme.fg_focus

theme.mstab_bar_padding = "default"         -- how much padding there should be between clients and your tabbar
-- theme.mstab_tabbar_position = "bottom"         -- position of the tabbar (mstab currently does not support left,right)
-- theme.mstab_tabbar_style = "default"        -- style of the tabbar ("default", "boxes" or "modern")
-- defaults to the tabbar_style so only change if you want a
-- different style for mstab and tabbed

-- theme.tabbar_bar_ontop = false                -- whether you want to allow the bar to be ontop of clients
theme.tabbar_dont_resize_slaves = false       -- whether the tabbed stack windows should be smaller than the
-- currently focused stack window (set it to true if you use
-- transparent terminals. False if you use shadows on solid ones
-- by default it will adjust based on your useless gaps. 
-- If you want a custom value. Set it to the number of pixels (int)



theme.master_width_factor = 0.60
-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
theme.taglist_bg = theme.base00
theme.taglist_shape = gears.shape.rounded_bar

-- Generate taglist squares:
local taglist_square_size = dpi(0)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
   taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
   taglist_square_size, theme.fg_normal
)


-- mstab
-- theme.mstab_bar_ontop = false                -- whether you want to allow the bar to be ontop of clients
-- theme.mstab_dont_resize_slaves = false       -- whether the tabbed stack windows should be smaller than the
-- currently focused stack window (set it to true if you use
-- transparent terminals. False if you use shadows on solid ones
-- by default it will adjust based on your useless gaps. 
-- If you want a custom value. Set it to the number of pixels (int)

-- theme.tabbar_bg_normal = theme.bg_normal
-- theme.tabbar_fg_normal = theme.fg_normal
-- theme.tabbar_bg_focus = theme.bg_focus
-- theme.tabbar_fg_focus = theme.fg_focus
-- theme.mstab_border_radius = 30              -- border radius of the tabbar
-- theme.mstab_bar_padding = "default"         -- how much padding there should be between clients and your tabbar
-- theme.mstab_tabbar_position = "top"         -- position of the tabbar (mstab currently does not support left,right)
-- theme.mstab_tabbar_style = "default"        -- style of the tabbar ("default", "boxes" or "modern")
-- defaults to the tabbar_style so only change if you want a
-- different style for mstab and tabbed




-- Tasklists
theme.tasklist_shape = gears.shape.rounded_bar
theme.tasklist_spacing = 40
theme.tasklist_align = "center"

theme.tasklist_bg = theme.red


-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
theme.notification_margin = dpi(10)
theme.notification_border_width = 0
theme.notification_shape = gears.shape.rounded_rect
theme.notification_position = "bottom_middle"
theme.notification_spacing = 10.0
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path.."default/submenu.png"
theme.menu_height = dpi(15)
theme.menu_width  = dpi(100)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"


-- wibar
theme.wibar_shape = gears.shape.rounded_bar
theme.wibar_bg = theme.bg_normal
theme.wibar_fg = theme.fg_normal
theme.wibar_y = 1030
theme.wibar_x = 0
-- theme.wibar_stretch = false
theme.wibar_border_width = dpi(0)
theme.wibar_border_color = ""
-- theme.wibar_ontop = nil
-- theme.wibar_cursor = nil
theme.wibar_opacity = 0.6
-- theme.wibar_type = nil
theme.wibar_width = dpi(1850)
-- theme.wibar_height = dpi(20)
-- theme.wibar_bgimage = nil

-- Define the image to load
-- theme.titlebar_bg_normal = "#282A36FF"
-- theme.titlebar_bg_focus = "#282A3699"
-- theme.titlebar_close_button_normal = themes_path.."default/titlebar/close_normal.png"
-- theme.titlebar_close_button_focus  = themes_path.."default/titlebar/close_focus.png"

-- theme.titlebar_minimize_button_normal = themes_path.."default/titlebar/minimize_normal.png"
-- theme.titlebar_minimize_button_focus  = themes_path.."default/titlebar/minimize_focus.png"

-- theme.titlebar_ontop_button_normal_inactive = themes_path.."default/titlebar/ontop_normal_inactive.png"
-- theme.titlebar_ontop_button_focus_inactive  = themes_path.."default/titlebar/ontop_focus_inactive.png"
-- theme.titlebar_ontop_button_normal_active = themes_path.."default/titlebar/ontop_normal_active.png"
-- theme.titlebar_ontop_button_focus_active  = themes_path.."default/titlebar/ontop_focus_active.png"

-- theme.titlebar_sticky_button_normal_inactive = themes_path.."default/titlebar/sticky_normal_inactive.png"
-- theme.titlebar_sticky_button_focus_inactive  = themes_path.."default/titlebar/sticky_focus_inactive.png"
-- theme.titlebar_sticky_button_normal_active = themes_path.."default/titlebar/sticky_normal_active.png"
-- theme.titlebar_sticky_button_focus_active  = themes_path.."default/titlebar/sticky_focus_active.png"

-- theme.titlebar_floating_button_normal_inactive = themes_path.."default/titlebar/floating_normal_inactive.png"
-- theme.titlebar_floating_button_focus_inactive  = themes_path.."default/titlebar/floating_focus_inactive.png"
-- theme.titlebar_floating_button_normal_active = themes_path.."default/titlebar/floating_normal_active.png"
-- theme.titlebar_floating_button_focus_active  = themes_path.."default/titlebar/floating_focus_active.png"

-- theme.titlebar_maximized_button_normal_inactive = themes_path.."default/titlebar/maximized_normal_inactive.png"
-- theme.titlebar_maximized_button_focus_inactive  = themes_path.."default/titlebar/maximized_focus_inactive.png"
-- theme.titlebar_maximized_button_normal_active = themes_path.."default/titlebar/maximized_normal_active.png"
-- theme.titlebar_maximized_button_focus_active  = themes_path.."default/titlebar/maximized_focus_active.png"

theme.wallpaper = wallpaperdir .. "RoyalKing.png"

-- You can use your own layout icons like this:
theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
theme.layout_max = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"
theme.layout_mstab = configdir.."bling/icons/layouts/mstab.png"

-- Notifications

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
   theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- theme.awesome_icon = theme.icons .. 'awesome.svg'
-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
