(local awful (require "awful"))
(local gears (require "gears"))
(local beautiful (require "beautiful"))
(local keybindings (require "keybindings"))
(local xresources (require "beautiful.xresources"))
(local dpi xresources.apply_dpi)
(local ruled (require "ruled"))

(local rules [
              ;; All notifications match this rule
              {
               :rule { }
               :propertites {
                             :border-width beautiful.border_width
                             :border_color "#000000"
                             :opacity 0.3
                             :shape gears.shape.rounded_rect
                             :position "bottom_middle"
                             }
               }
              ])

rules
