(local awful (require "awful"))
(local naughty (require "naughty"))
(local gears (require "gears"))
(local beautiful (require "beautiful"))
(local xresources (require "beautiful.xresources"))
(local dpi xresources.apply_dpi)

(local file (io.open "/etc/hostname"))
(io.input file)
(local laptop (if (= (io.read) "syl")
                  true
                  false))
(io.close file)

(local rules [
              ;; All clients match this rule
              {
               :rule {  }
               :propertites {
                             :border_width beautiful.border_width
                             :border_color beautiful.border_normal
                             :focus awful.client.focus.filter
                             :raise true
                             ;; :keys keys
                             ;; :buttons clientbuttons
                             :screen awful.screen.preferred
                             :size_hints_honor false
                             ;; :placement awful.placement.no_overlap
                             :shape_clip gears.shape.rounded_rect 
                             :shape_bounding gears.shape.rounded_rect 
                             }
               }

              ;; floating and centered videos from mpv or firefox pnp
              {
               :rule_any {
                          :class [
                                  "mpv"
                                  ]
                          :instance [
                                     "mpv"
                                     ]
                          :name [
                                 "Picture-in-Picture"
                                 ]
                          }
               :properties {
                            :floating true
                            :raise true
                            ;; :height (dpi 1200)
                            :width (if laptop (dpi 1400)
                                       (dpi 2000))
                            :screen 1
                            :sticky true
                            :placement awful.placement.centered
                            ;; :size_hints_honor false
                            }
               }
              {
               :rule_any {
                          :name [
                                 "webcam"
                                 ]
                          }
               :properties {
                            :floating true
                            :raise true
                            :height (dpi 400)
                            :screen (if (= (screen.count) 3)
                                        2
                                        (screen.count))
                            :sticky true
                            :ontop true
                            :placement awful.placement.centered
                            }
               }
              ;; Davinci Resolve is fullscreen
              {
               :rule_any {
                          :instance [
                                     "resolve"
                                     ]
                          :class [
                                  "resolve"
                                  ]
                          }
               :properties {
                            :floating true
                            :fullscreen true

                            }
               }
              {
               :rule_any {
                          :name [
                                 "Secondary Screen"
                                 ]
                          }
               :properties {
                            :screen (screen.count)
                            :floating true
                            :fullscreen true
                            :size_hints_honor false
                            }
               }
              ;; sxiv floating on main screen for quick picture viewing
              {
               :rule_any {
                          :class [
                                  "sxiv"
                                  "Sxiv"
                                  "imv"
                                  "Imv"
                                  ]
                          }
               :properties {
                            :floating true
                            :raise true
                            :height (dpi 800)
                            :width (dpi 1200)
                            :placement awful.placement.centered
                            }
               }
              ;; Emacs
              {
               :rule_any {
                          :instance [
                                     "emacs"
                                     "scribus"
                                     ]
                          :class [
                                  "scribus"
                                  ]
                          }
               :properties {
                            :size_hints_honor false
                            }
               }
              ;; Org-Agenda
              {
               :rule_any {
                          :name [
                                 "org-agenda"
                                 ]
                          :class [
                                  "org-agenda"
                                  ]
                          :instance [
                                     "org-agenda"
                                     ]
                          }
               :properties {
                            :floating true
                            :raise true
                            :sticky true
                            :placement (+ awful.placement.no_offscreen awful.placement.centered)
                            }
               }
              ;; Org Capture
              {
               :rule_any {
                          :name [
                                 "org-capture"
                                 ]
                          }
               :properties {
                            :floating true
                            :raise true
                            :screen awful.screen.preferred
                            :placement (+ awful.placement.no_offscreen awful.placement.centered)
                            }
               }
              ;; Pulsemixer center and smaller
              {
               :rule_any {
                          :instance [
                                     "pulsemixer"
                                     ]
                          :class [
                                  ]
                          }
               :properties {
                            :floating true
                            :raise true
                            :ontop true
                            :screen awful.screen.preferred
                            ;; :width (dpi 600)
                            ;; :height (dpi 200)
                            :placement awful.placement.centered
                            }
               }
              {
               :rule_any {
                          :instance [
                                     "btop"
                                     ]
                          :class [
                                  "btop"
                                  ]
                          }
               :properties {
                            :floating true
                            :raise true
                            :ontop true
                            :screen awful.screen.preferred
                            :width (dpi 1000)
                            :height (dpi 700)
                            :placement awful.placement.centered
                            }
               }
              {
               :rule_any {
                          :instance [
                                     "eww"
                                     ]
                          :class [
                                  "eww"
                                  ]
                          }
               :properties {
                            :floating true
                            :raise false
                            :ontop true
                            :screen awful.screen.preferred
                            :focusable false
                            ;; :width (dpi 600)
                            ;; :height (dpi 200)
                            :placement (+ awful.placement.centered awful.placement.no_offscreen awful.placement.top)
                            :y (dpi 100)
                            }
               }
              ;; Tell qb to open primarily on the secondary monitor
              {
               :rule_any {
                          :class [
                                  "qutebrowser"
                                  "nyxt"
                                  ]
                          :instance [
                                     "nyxt"
                                     ]
                          }
               :properties {
                            :screen (if (= (screen.count) 3)
                                        2
                                        (screen.count))
                            :focus awful.client.focus.filter
                            :raise true
                            }
               }
              ;; floating windows
              {
               :rule_any {
                          :class [
                                  "Libre Presenter"
                                  ]}
               :properties {
                            :floating true
                            :raise true
                            :screen awful.screen.preferred
                            :height (dpi 850)
                            :width (dpi 1450)
                            :placement awful.placement.centered
                            }
               }
              {
               :rule_any {
                          :class [
                                  "dolphin"
                                  "feh"
                                  "Arandr"
                                  "gcr-prompter"
                                  "ffplay"
                                  "Junction"
                                  "junction"
                                  "Libre Presenter"
                                  "pinentry-qt"
                                  ]
                          :name [
                                 "Event Tester"
                                 "remove images?"
                                 "Blender Preferences"
                                 "Unlock Login Keyring"
                                 ]
                          :role [
                                 "pop-up"
                                 "GtkFileChooserDialog"
                                 ]
                          :instance [
                                     "junction"
                                     "Junction"
                                     ]}
               :properties {
                            :floating true
                            :raise true
                            :screen awful.screen.preferred
                            :placement (+ awful.placement.centered awful.placement.no_offscreen)
                            }
               }
              {
               :rule_any {
                          :class [
                                  "fp64"
                                  "vlc"
                                  "Vlc"
                                  "OpenLP"
                                  ]
                          :instance [
                                     "fp64"
                                     "vlc"
                                     "Vlc"
                                     "OpenLP"
                                     ]
                          :name [
                                 "Display Window"
                                 "OpenLP"
                                 ]
                          }
               :except_any {
                            :name [
                                   "Configure OpenLP"
                                   "OpenLP - "
                                   ]
                            }
               :properties {
                            :fullscreen true
                            :raise true
                            :ontop true
                            :screen (screen.count)
                            :placement (+ awful.placement.no_offscreen awful.placement.centered)
                            }
               }
              ])

rules
