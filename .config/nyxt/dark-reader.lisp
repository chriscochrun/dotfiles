(in-package #:nyxt-user)

(define-configuration nx-dark-reader:dark-reader-mode
  ((nxdr:brightness 80)
   (nxdr:contrast 80)
   (nxdr:text-color "white")))

;; Add dark-reader to default modes
;; (define-configuration web-buffer
;;   ((default-modes `(nxdr:dark-reader-mode ,@%slot-value%))))
