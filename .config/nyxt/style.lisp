(in-package #:nyxt-user) ; While implicit, this allows SLY to know which package we are in.

(setf (uiop:getenv "GTK_THEME") "Adwaita:dark")

(define-configuration browser
  "Configuring my reddish theme."
  ((theme (make-instance
           'theme:theme
           :background-color "#282a36"
           :on-background-color "#e2e4e5"
           :accent-color "#5af78e"
           :on-accent-color "#282a36"
           :accent-alt-color "#9aedfe"
           :on-accent-alt-color "#282a36"
           :warning-color "#ff5c57"
           :on-warning-color "#e2e4e5"
           :primary-color "#43454f"
           :on-primary-color "#57c7ff"
           :on-secondary-color "#f3f99d"
           :secondary-color "#282a36"
           :font-family "VictorMono Nerd Font"))))

(define-configuration :dark-mode
  "Dark-mode is a simple mode for simple HTML pages to color those in a darker palette.

I don't like the default gray-ish colors, though. Thus, I'm overriding
those to be a bit more laconia-like.

I'm not using this mode, though: I have nx-dark-reader."
  ((style
    (theme:themed-css (theme *browser*)
                      `(*
                        :background-color ,(if (theme:dark-p theme:theme)
                                               theme:background
                                             theme:on-background)
                        "!important"
                        :background-image none "!important"
                        :color ,(if (theme:dark-p theme:theme)
                                    theme:on-background
                                  theme:background)
                        "!important")
                      `(a
                        :background-color ,(if (theme:dark-p theme:theme)
                                               theme:background
                                             theme:on-background)
                        "!important"
                        :background-image none "!important"
                        :color ,theme:primary "!important")))))

(define-configuration :hint-mode
  ((style
    (theme:themed-css (theme *browser*)
      `(".nyxt-hint" :background-color ,theme:accent :color
                     ,theme:on-accent :font-family "monospace,monospace" :padding
                     "0px 0.3em" :border-color ,theme:primary :border-radius "0.4em"
                     :border-width "0.2em" :border-style "solid" :z-index 2147483647)
      `(".nyxt-hint.nyxt-mark-hint" :background-color ,theme:secondary :color
                                    ,theme:on-secondary :font-weight "bold")
      `(".nyxt-hint.nyxt-select-hint" :background-color ,theme:on-primary :color
                                      ,theme:primary)
      `(".nyxt-hint.nyxt-match-hint" :padding "0px" :border-style "none" :opacity
                                     "0.5")
      `(".nyxt-element-hint" :background-color ,theme:on-primary)))))

;; (define-configuration :buffer
;;   ((style
;;     (theme:themed-css (theme *browser*)
;;       `("h1,h2,h3,h4,h5,h6" :color ,theme:on-primary)
;;       `(.button :background-color ,theme:primary
;;                 :color ,theme:on-primary
;;                 :border-radius "0.6em")
;;       `(.link :color ,theme:on-primary)
;;       `(a :color ,theme:on-primary))))
