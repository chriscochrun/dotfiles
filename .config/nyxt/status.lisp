(in-package #:nyxt-user)

;;allow setting glyphs
(define-configuration status-buffer
  ((glyph-mode-presentation-p t)))

;; ;;various glyph settings with no additional configs
(define-configuration :force-https-mode ((glyph "ϕ")))
(define-configuration :blocker-mode ((glyph "β")))
(define-configuration :proxy-mode ((glyph "π")))
(define-configuration :reduce-tracking-mode ((glyph "∅")))
(define-configuration :certificate-exception-mode ((glyph "ɛ")))
(define-configuration :style-mode ((glyph "s")))
(define-configuration :user-script-mode ((glyph "ω")))

(define-configuration status-buffer
  ((style (str:concat %slot-value%
                      (theme:themed-css (theme *browser*)
                        ;; See the `describe-class' of `status-buffer' to
                        ;; understand what to customize
                        '("#controls"
                          :display "none"
                          :important))))))
