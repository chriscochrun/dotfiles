{
    "layer": "top", // Waybar at top layer
    // "output": "DP-1",
    "position": "bottom", // Waybar position (top|bottom|left|right)
    "height": 35, // Waybar height (to be removed for auto height)
    "width": 1400, // Waybar width
    // Choose the order of the modules "custom/wintitle", 
    "modules-left": ["hyprland/workspaces", "hyprland/window"],
    "modules-center": ["clock"],
    "modules-right": ["pulseaudio", "backlight", "memory", "cpu", "battery", "tray"],
    "margin-top": -5,
    "margin-bottom": 8,
    "margin-left": 6,
    "margin-right": 6,
    // Modules configuration
    "hyprland/window": {
        "format": "  <span style='italic'>{}</span>"
    },
    "hyprland/workspaces": {
     "format": "{icon}",
     "format-icons": {
     "1": "",
     "2": "",
     "3": "󰈙",
     "4": "󰍨",
     "5": "",
     "9": "",
     "active": "",
     "default": ""
     },
     // "on-scroll-up": "hyprctl dispatch workspace e+1",
     // "on-scroll-down": "hyprctl dispatch workspace e-1"
    },
    "sway/mode": {
        "format": "<span style=\"italic\">  {}</span>"
    },
    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    },
    "tray": {
        "icon-size": 21,
        "spacing": 10
    },
    "clock": {
        // "timezone": "America/New_York",
        "tooltip-format": "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>",
        "format-alt": "{:%Y-%m-%d}",
        "format": "{:%a %b %e, %I:%M %p}"
    },
    "cpu": {
        "format": "  {usage}%",
    },
    "memory": {
        "format": "  {}%"
    },
    "temperature": {
        // "thermal-zone": 2,
        // "hwmon-path": "/sys/class/hwmon/hwmon2/temp1_input",
        "critical-threshold": 80,
        // "format-critical": "{temperatureC}°C {icon}",
        "format": "{icon} {temperatureC}°C",
        "format-icons": ["", "", ""]
    },
    "backlight": {
        // "device": "acpi_video1",
        "format": "{icon}  {percent}%",
        "format-icons": ["", ""]
    },
    "battery": {
        "states": {
            "good": 95,
            "warning": 30,
            "critical": 15
        },
        "format": "{icon} {capacity}%",
        "format-charging": "{icon} {capacity}%",
        "format-plugged": " {capacity}%",
        "format-alt": "{time} {icon}",
        "format-good": "{icon} {capacity}%", // An empty format will hide the module
        "format-full": "󰁹 {capacity}%",
        "format-icons": ["󰁺", "󰁼", "󰁾", "󰂀", "󰂂"],
        "format-charging-icons": ["󰢜", "󰂇", "󰢝", "󰢞", "󰂋"]
    },
    "battery#bat2": {
        "bat": "BAT2",
        "states": {
            // "good": 95,
            "warning": 30,
            "critical": 15
        },
        "format": "{icon} {capacity}%",
        "format-charging": "{charging-icons} {capacity}%",
        "format-plugged": " {capacity}%",
        "format-alt": "{time} {icon}",
        // "format-good": "", // An empty format will hide the module
        // "format-full": "",
        "format-icons": [" ", " ", " ", " ", " "],
        "format-charging-icons": [" ", " ", " ", " ", " "]
    },
    "network": {
        // "interface": "wlp2*", // (Optional) To force the use of this interface
        "format-wifi": "  ({signalStrength}%)",
        "format-ethernet": " {ipaddr}",
        "format-linked": "{ifname} (No IP) ",
        "format-disconnected": "Disconnected ⚠",
        "format-alt": "{ifname}: {essid} {ipaddr}/{cidr}",
    },
    "pulseaudio": {
        // "scroll-step": 1, // %, can be a float
        "format": "{icon} {volume}% {format_source}",
        "format-bluetooth": "{icon} {volume}% {format_source}",
        "format-bluetooth-muted": " {icon} {format_source}",
        "format-muted": "ﱝ muted",
        "format-source": " {volume}%",
        "format-source-muted": "",
        "format-icons": {
            "headphone": "",
            "hands-free": "",
            "headset": "",
            "phone": "",
            "portable": "",
            "car": "",
            "default": ["", "", ""]
        },
        "on-click": "alacritty --class pulsemixer -e pulsemixer"
    },
    "custom/media": {
        "format": "{icon} {}",
        "return-type": "json",
        "max-length": 40,
        "format-icons": {
            "spotify": "",
            "default": "🎜"
        },
        "escape": true,
        "exec": "$HOME/.config/waybar/mediaplayer.py 2> /dev/null" // Script in resources folder
        // "exec": "$HOME/.config/waybar/mediaplayer.py --player spotify 2> /dev/null" // Filter player based on name
    },
    "custom/wintitle": {
        "format": "  {icon} {}",
        "max-length": 80,
        //"format-icons": {
        //    "spotify": "",
        //    "default": "🎜"
        //},
        "interval": 1,
        "exec": "/home/chris/bin/wintitle"
    },
    "wlr/taskbar": {
   	"format": "{icon}",
   	"icon-size": 20,
   	"icon-theme": "Papirus-Dark",
   	"tooltip-format": "{title}",
	"max-length": 10,
   	"on-click": "activate",
   	"on-click-middle": "close"
    },
    "sway/window": {
    	"format": "{} ",
	"max-length": 60,
    }
}
