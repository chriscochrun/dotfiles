#!/usr/bin/env bash

if [ $(hostname) = "syl" ]; then
    style="laptop"
    #echo "this is hidpi"
else 
    style="desktop"
    #echo "this is not hidpi"
fi

rofi -no-lazy-grab -show emoji -modi emoji -theme launchers-git/"$style".rasi



# $@ &
# c=0
# while ! xprop -f _KDE_NET_WM_BLUR_BEHIND_REGION 32c -set _KDE_NET_WM_BLUR_BEHIND_REGION 0 -id $(xdotool search -class 'rofi') ; do
#     sleep .1 
#     c=$((c+1))
#     [[ c = 50 ]] && exit; # stop script window didn't appear after 5 seconds
# done

