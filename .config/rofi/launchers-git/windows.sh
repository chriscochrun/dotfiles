#!/usr/bin/env bash

if [ $(hostname) = "syl" ]; then
    style="laptop"
    #echo "this is hidpi"
else 
    style="desktop"
    #echo "this is not hidpi"
fi

rofi -no-lazy-grab -show window -modi window -window-command "wmctrl -i -a {window}" -theme launchers-git/"$style".rasi
