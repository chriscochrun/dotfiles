(defproject ags-config "0.0.1"
  :dependencies [[org.clojure/clojurescript "1.11.54"]
                 [org.clojure/clojure "1.11.3"]]
  :plugins [[lein-cljsbuild "1.1.8"]]
  :cljsbuild {
    :builds [{
        ; The path to the top-level ClojureScript source directory:
        :source-paths ["src"]
        ; The standard ClojureScript compiler options:
        ; (See the ClojureScript compiler documentation for details.)
        :compiler {
          :output-to "main.js"  ; default: target/cljsbuild-main.js
          :optimizations :simple
          :pretty-print true}}]})
