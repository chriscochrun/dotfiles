const divide = ([total, free]) => free / total

const cpu = Variable(0, {
    poll: [2000, 'top -b -n 1', out => divide([100, out.split('\n')
        .find(line => line.includes('Cpu(s)'))
        .split(/\s+/)[1]
        .replace(',', '.')])],
})

const ram = Variable(0, {
    poll: [2000, 'free', out => divide(out.split('\n')
        .find(line => line.includes('Mem:'))
        .split(/\s+/)
        .splice(1, 2))],
})

export const cpuProgress = Widget.CircularProgress({
    value: cpu.bind(),
    rounded: true,
    start_at: 0.75,
    class_name: cpu.bind().as(cpu => {
        switch(true) {
        case (cpu < .25):
            return "cpu_low";
            break;
        case (cpu < .7):
            return "cpu";
            break;
        default:
            return "cpu_high";
            break;
        }
    }),
    child: Widget.Icon({
        class_name: "cpu_icon",
        icon: "cpu-symbolic",
    }),
    tooltip_text: cpu.bind().as(cpu => cpu * 100 + "%"),
})

export const ramProgress = Widget.CircularProgress({
    value: ram.bind(),
    rounded: true,
    start_at: 0.75,
    class_name: ram.bind().as(ram => {
        switch(true) {
        case (ram < .25):
            return "ram_low";
            break;
        case (ram < .7):
            return "ram";
            break;
        default:
            return "ram_high";
            break;
        }
    }),
    child: Widget.Icon({
        class_name: "ram_icon",
        icon: "ram-symbolic",
    }),
    tooltip_text: ram.bind().as(ram => ram * 100 + "%"),
})

// export function cpuProgress() {
//     return Widget.CircularProgress({
//         value: cpu.bind(),
//         rounded: true,
//         start_at: 0.75,
//         class_name: "cpu",
//     })
// }
