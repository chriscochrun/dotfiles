const css = `${App.configDir}/style.css`;
const battery = await Service.import("battery");

const laptop = Utils.exec(`hostname`) === "syl";

const status = Variable(true);

export function BatteryPopup(monitor = laptop ? 0 : 2) {
    const battery_percent = battery.bind("percent");
    // status.value = battery.connect("charging");
    console.log(status);
    return Widget.Window({
        monitor,
        name: `battery${monitor}`,
        class_name: "battery-popup",
        anchor: [laptop ? "bottom" : "top"],
        css: "margin-right: 30px",
        visible: battery_percent.as(p => p < 25 ? charging.as(c => !c) : false),
        child: Widget.Box({
            // css: "padding: .8em",
            children: [
                Widget.Icon({
                    css: "padding: .8em",
                    class_name: "battery-icon",
                    icon: battery.bind("icon-name"),
                }),
                Widget.Label({
                    css: "padding: .8em",
                    label: battery.bind("percent").as(p => p + "% Please charge me!!"),
                }),
                Widget.Button({
                    css: "font-size: 60px;",
                    class_name: "battery-close",
                    child: Widget.Icon({
                        icon: "view-close",
                    }),
                    on_clicked: () => console.log("lets' close"),
                })
            ],
        }),
    })
}
