{ config, lib, pkgs, ags, ... }:

let
  laptop = builtins.readFile "/etc/hostname" == "syl\n";
in
{
  imports = [
    ./modules/hyprland.nix
    ags.homeManagerModules.default
  ];
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "chris";
  home.homeDirectory = "/home/chris";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.11"; # Did you read the comment?

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  xdg.userDirs = {
    enable = true;
    desktop = "${config.home.homeDirectory}";
    documents = "${config.home.homeDirectory}/docs";
    download = "${config.home.homeDirectory}/dls";
    music = "${config.home.homeDirectory}/music";
    pictures = "${config.home.homeDirectory}/pics";
    publicShare = "${config.home.homeDirectory}";
    templates = "${config.home.homeDirectory}";
    videos = "${config.home.homeDirectory}/vids";
  };

  xdg.portal = {
    enable = true;
    config = {
      common = {
        default = [
          pkgs.xdg-desktop-portal-kde
        ];
        "org.freedesktop.impl.portal.FileChooser" = [ "kde" ];
      };
      hyprland = {
        default = [
          "hyprland"
        ];
        "org.freedesktop.impl.portal.FileChooser" = [ "kde" ];
      };
    };
    extraPortals = [ pkgs.xdg-desktop-portal-kde ];
  };

  # home.file.".config/xdg-desktop-portal/portals.conf" = {
  #   text = ''
  #   [preferred]
  #   org.freedesktop.impl.portal.FileChooser=kde
  #   '';
  # };

  programs.gpg = {
    enable = true;
  };

  services.gpg-agent = {
    enable = true;
    pinentryPackage = pkgs.pinentry-qt;
    # extraConfig = ''
    #   pinentry-program ${pkgs.pinentry.qt}/bin/pinentry
    # '';
  };

  accounts.email = {
    maildirBasePath = "mail";
    accounts = {
      personal = {
        address = "chris@cochrun.xyz";
        userName = "chris@cochrun.xyz";
        mbsync.enable = true;
        mu.enable = true;
        flavor = "plain";
        primary = true;
        passwordCommand = "${pkgs.rbw}/bin/rbw get 'Office 365'";
        realName = "Chris Cochrun";
        signature = {
          text = ''
          Praising God in all things,
          Chris Cochrun
          '';
          delimiter = ''
          ***
          '';
          showSignature = "append";
        };
        imap = {
          host = "mail.cochrun.xyz";
          port = 993;
          tls.enable = true;
        };
        smtp = {
          host = "mail.cochrun.xyz";
          port = 25;
          tls.enable = true;
        };
        mbsync = {
          create = "maildir";
          remove = "both";
          expunge = "both";
        };
        maildir.path = "cochrun";
      };

      work = {
        address = "chris@tfcconnection.org";
        userName = "chris@tfcconnection.org";
        mbsync.enable = true;
        mu.enable = true;
        flavor = "outlook.office365.com";
        passwordCommand = "cat ~/.config/access";
        realName = "Chris Cochrun";
        imap = {
          host = "outlook.office365.com";
          port = 993;
          tls.enable = true;
        };
        smtp = {
          host = "smtp.office365.com";
          port = 587;
          tls.enable = true;
        };
        mbsync = {
          create = "maildir";
          remove = "both";
          expunge = "both";
          extraConfig.account = {
            AuthMechs = "XOAUTH2";
          };
        };
        maildir.path = "office";
        signature = {
          text = ''
          Praising God in all things,
          Chris Cochrun
          '';
          delimiter = ''
          ***
          '';
          showSignature = "append";
        };
      };
    };
  };

  programs.git = {
    enable = true;
    userName = "Chris Cochrun";
    userEmail = "chris@cochrun.xyz";
  };

  home.packages = with pkgs; [
  ];

  programs.mu.enable = true;

  programs.mbsync = {
    package = pkgs.isync.override { withCyrusSaslXoauth2 = true; };
    enable = true;
  };
  programs.msmtp.enable = true;
  services.mbsync.enable = true;

  programs.obs-studio = {
    enable = true;
    plugins = [ pkgs.obs-studio-plugins.obs-move-transition ];
  };

  home.file.".config/mpv" = {
    source = ../.config/mpv;
  };

  # home.file.".config/ags" = {
  #   source = ../.config/ags;
  # };
  # services.kdeconnect.enable = true;

  services = {
    syncthing.enable = true;
    easyeffects.enable = true;
    nextcloud-client = {
      enable = true;
      startInBackground = true;
    };

    mpd.enable = true;
    pueue.enable = true;

    # For matrix clients that don't have e2ee
    pantalaimon = {
      enable = true;
      settings = {
        Default = {
          LogLevel = "Debug";
          SSL = true;
        };
        local-matrix = {
          Homeserver = "https://matrix.tfcconnection.org";
          ListenAddress = "127.0.0.1";
          ListenPort = 8008;
          # SSL = true;
          # IgnoreVerification = true;
        };
      };
    };
  };

  # stylix = {
  #   enable = true;
  #   polarity = "dark";
  #   base16Scheme = {
  #     base00 = "282a36";
  #     base01 = "34353e";
  #     base02 = "43454f";
  #     base03 = "78787e";
  #     base04 = "a5a5a9";
  #     base05 = "e2e4e5";
  #     base06 = "eff0eb";
  #     base07 = "f1f1f0";
  #     base08 = "ff5c57";
  #     base09 = "ff9f43";
  #     base0A = "f3f99d";
  #     base0B = "5af78e";
  #     base0C = "9aedfe";
  #     base0D = "57c7ff";
  #     base0E = "ff6ac1";
  #     base0F = "b2643c";
  #   };
  #   image = config.lib.stylix.pixel "base0D";
  #   # targets.tofi.enable = false;
  # };

  # programs.tofi.enable = true;

 #services.espanso = {
 #  enable = true;
 #  settings = {
 #    toggle_key = "RIGHT_CTRL";
 #    matches = [
 #      { # dates
 #        trigger = ":date";
 #        replace = "{{mydate}}";
 #        vars = [{

 #          name = "mydate";
 #          type = "date";
 #          params = {format = "%m/%d/%Y";};
 #        }];
 #      }
 #      { # Shell commands
 #        trigger = ":shell";
 #        replace = "{{output}}";
 #        vars = [{
 #          name = "output";
 #          type = "shell";
 #          params = { cmd = "echo Hello from your shell";};
 #        }];
 #      }
 #      { # simple text
 #        trigger = ":gml";
 #        replace = "ccochrun21@gmail.com";
 #      }
 #      {
 #        trigger = ":otl";
 #        replace = "chris.cochrun@outlook.com";
 #      }
 #      {
 #        trigger = ":tfcml";
 #        replace = "chris@tfcconnection.org";
 #      }
 #      {
 #        trigger = ":name";
 #        replace = "Chris Cochrun";
 #      }
 #      {
 #        trigger = ":cn";
 #        replace = "A Giant Gummy Lizard";
 #      }
 #    ];
 #  };
 #};

  home.pointerCursor = {
    gtk.enable = true;
    package = pkgs.phinger-cursors;
    name = "phinger-cursors-light";
    size = 32;
  };

  dconf.settings = {
    "org/gnome/desktop/interface" = {
      color-scheme = "prefer-dark";
    };
  };

  gtk = {
    enable = true;
    cursorTheme = {
      name = "phinger-cursors-light";
      package = pkgs.phinger-cursors;
      size = 32;
    };
    font = {
      name = "VictorMono Nerd Font";
      size = 11;
    };
    iconTheme = {
      name = "Papirus-Dark";
      package = pkgs.papirus-icon-theme;
    };
    theme.name = "Breeze";
    gtk3.extraConfig = {
      gtk-application-prefer-dark-theme = true;
      gtk-button-images = true;
      gtk-decoration-layout = "icon:minimize,maximize,close";
      gtk-enable-animations = true;
      gtk-enable-event-sounds = 1;
      gtk-menu-images = true;
      gtk-primary-button-warps-slider = true;
      gtk-xft-antialias = 1;
      gtk-xft-dpi = 98304;
      gtk-xft-hinting = 1;
      gtk-xft-hintstyle = "hintfull";
      gtk-xft-rgba = "rgb";
    };
  };

  services.cliphist = {
    enable = true;
  };

  home.file.".config/bottom" = {
    source = ../.config/bottom;
    recursive = true;
  };

  home.file.".config/rofi" = {
    source = ../.config/rofi;
    recursive = true;
  };

  programs.firefox = {
    enable = true;
    package = pkgs.firefox-wayland.override {
      nativeMessagingHosts = [
        pkgs.plasma-browser-integration
        pkgs.tridactyl-native
      ];
    };
    profiles.chris = {
      name = "default";
      path = "nw77o6yc.default";
      isDefault = true;
    };
  };

  programs.waybar = {
    enable = true;
    settings = {
      mainBar = {
        layer = "top";
        output = ["DP-1" "eDP-1"];
        position = if laptop then "bottom" else "top";
        height = 35;
        width = if laptop then 1300 else 2100;
        # Choose the order of the modules "custom/wintitle", 
        modules-left = ["hyprland/workspaces" "hyprland/window"];
        modules-center = ["clock"];
        modules-right = ["pulseaudio" "backlight" "disk" "memory" "cpu" "battery" "tray"];
        margin-top = if laptop then -5 else 5;
        margin-bottom = if laptop then 8 else 0;
        margin-left = 6;
        margin-right = 6;
        "hyprland/window" = {
          format = "  <span style='italic'>{}</span>";
        };
        workspaces = {
          format = " {name} ";
        };
        "hyprland/workspaces" = {
          format = "{icon}";
          format-icons = {
            "1" = "";
            "2" = "";
            "3" = "󰈙";
            "4" = "󰭹";
            "5" = "";
            "6" = "";
            "7" = "󰕧";
            "8" = "󰭹";
            "9" = "";
          };
          all-outputs = true;
          # on-scroll-up = "hyprctl dispatch workspace e+1";
          # on-scroll-down = "hyprctl dispatch workspace e-1";
        };
        tray = {
          icon-size = 21;
          spacing = 10;
        };
        clock = {
          tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
          format-alt = "{:%Y-%m-%d}";
          format = "{:%a %b %e, %I:%M %p}";
        };
        cpu = {
          format = "  {usage}%";
        };
        memory = {
          format = "  {}%";
        };
        temperature = {
          # "thermal-zone": 2,
          # "hwmon-path": "/sys/class/hwmon/hwmon2/temp1_input",
          critical-threshold = 80;
          # "format-critical": "{temperatureC}°C {icon}",
          format = "{icon} {temperatureC}°C";
          format-icons = ["" "" ""];
        };
        backlight = {
          # "device": "acpi_video1",
          format = "{icon}  {PERCENT}%";
          format-icons = ["" ""];
        };
        disk = {
          format = "󰋊 {percentage_used}%";
          tooltip-format = "{used} out of {total} with {free} left";
        };
        battery = {
          states = {
            good = 95;
            warning = 30;
            critical = 15;
          };
          format = "{icon} {capacity}%";
          format-charging = "{icon} {capacity}%";
          format-plugged = " {capacity}%";
          format-alt = "{time} {icon}";
          format-good = "{icon} {capacity}%";
          format-full = "󰁹 {capacity}%";
          format-icons = ["󰁺" "󰁼" "󰁾" "󰂀" "󰂂"];
          format-charging-icons = ["󰢜" "󰂇" "󰢝" "󰢞" "󰂋"];
        };
        network = {
          # "interface": "wlp2*", // (Optional) To force the use of this interface
          format-wifi = "  ({signalStrength}%)";
          format-ethernet = "󰈀 {ipaddr}";
          format-linked = "{ifname} (No IP)";
          format-disconnected = "Disconnected ";
          format-alt = "{ifname}: {essid} {ipaddr}/{cidr}";
        };
        pulseaudio = {
          scroll-step = 1;
          format = "{icon} {volume}%";
          format-muted = "󰝟 muted";
          format-source = "";
          format-source-muted = "";
          format-icons = {
            headphone = ["" "" ""];
            phone = "";
            portable = "";
            car = "";
            default = ["" "" ""];
          };
          on-click = "alacritty --class pulsemixer -e pulsemixer";
          ignored-sinks = ["Easy Effects Sink"];
        };
      };
    };
    style = ''
@define-color base00 #282a36;
@define-color base01 #34353e;
@define-color base02 #43454f;
@define-color base03 #78787e;
@define-color base04 #a5a5a9;
@define-color base05 #e2e4e5;
@define-color base06 #eff0eb;
@define-color base07 #f1f1f0;
@define-color base08 #ff5c57;
@define-color base09 #ff9f43;
@define-color base0A #f3f99d;
@define-color base0B #5af78e;
@define-color base0C #9aedfe;
@define-color base0D #57c7ff;
@define-color base0E #ff6ac1;
@define-color base0F #b2643c;
@define-color basetransparent rgba(40, 42, 54, 0.0);
@define-color backtransparent rgba(40, 42, 54, 0.7);
* {
    border:        1px;
    border-radius: 20px;
    font-family:   VictorMono Nerd Font;
    font-size:     15px;
    font-weight:   normal;
    box-shadow:    none;
    text-shadow:   none;
    transition-duration: 0s;
    padding-top: 0px;
    padding-bottom: 0px;
}


window {
    background: transparent;
    /* border-radius: 20px; */
}

window#waybar > box {
    padding-top: 5px;
    padding-bottom: 3px;
    padding-left: 3px;
    padding-right: 10px;
    color:      @base05;
    box-shadow: 5px 5px 4px 4px #202020;
    margin: 12px 14px 14px 14px;
    background: @backtransparent;
}

tooltip {
  background: @base00;
  border: 1px solid @base0D;
}

tooltip label {
  padding: 10px;
}

#workspaces {
    border-radius: 20px;
    margin-left: 10px;
    padding-right: 10px;
    background: transparent;
    transition: none;
    color: @base0C;
}

#tags {
    border-radius: 20px;
    margin-left: 6px;
    padding-right: 10px;
    background: transparent;
    transition: none;
}

#workspaces button {
    padding: 0 0.7em;
    transition: none;
    color:      rgba(217, 216, 216, 0.4);
    background: transparent;
}

#tags button {
    transition: none;
    color:      rgba(217, 216, 216, 0.4);
    background: transparent;
}

#workspaces button.visible {
    color: @base0C;
}

#tags button.occupied {
    color:      rgba(217, 216, 216, 1);
}

#workspaces button.focused {
    color: @base09;
}

#tags button.focused {
    color: @base0C;
}

#workspaces button:hover {
    transition: none;
    box-shadow: inherit;
    text-shadow: inherit;
    color: @base0E;
}

#workspaces button.urgent {
    color:      @base08;
}

#mode, #battery, #cpu, #memory, #network, #pulseaudio, #idle_inhibitor, #backlight, #custom-storage, #disk, #custom-weather, #custom-mail {
    margin:     0px 3px 0px 3px;
    /* min-width:  25px; */
}

#clock {
    margin-left: 2px;
    margin-right: 2px;
    padding-left: 10px;
    padding-right: 10px;
    border-radius: 20px;
    transition: none;
    color: @base0B;
    background: transparent;
}

#backlight {
    margin-left: 2px;
    margin-right: 2px;
    padding-left: 5px;
    padding-right: 5px;
    border-radius: 20px;
    transition: none;
    color: @base08;
    background: transparent;
}

#battery.warning {
    color:       @base09;
}

#battery.critical {
    color:      @base08;
}

#battery.charging {
    color:      @base0B;
}

#pulseaudio {
    padding-left: 5px;
    padding-right: 5px;
    border-radius: 20px;
    transition: none;
    color: @base09;
    background: transparent;
}

#network {
    padding-left: 5px;
    padding-right: 5px;
    border-radius: 20px;
    transition: none;
    color: @base0B;
    background: transparent;
}

#cpu {
    padding-left: 5px;
    padding-right: 5px;
    border-radius: 20px;
    transition: none;
    color: @base09;
    background: transparent;
}

#battery {
    padding-left: 5px;
    padding-right: 5px;
    border-radius: 20px;
    transition: none;
    color: @base0D;
    background: transparent;
}

#battery.bat2 {
    margin-right: 10px;
}

#memory {
    padding-left: 5px;
    padding-right: 5px;
    border-radius: 20px;
    transition: none;
    color: @base0C;
    background: transparent;
}

#disk {
    padding-left: 5px;
    padding-right: 5px;
    border-radius: 20px;
    transition: none;
    color: @base0E;
    background: transparent;
}

#tray {
    padding-left: 15px;
    padding-right: 5px;
    border-radius: 20px;
    transition: none;
    color: @base0E;
    background: transparent;
}

    '';
  };

  home.file.".mozilla/firefox/nw77o6yc.default/chrome" = {
    source = ../.config/firefox/chrome;
    recursive = true;
  };

  home.file.".config/tridactyl" = {
    source = ../.config/tridactyl;
    recursive = true;
  };

  programs.rbw.enable = true;
  home.file.".config/rbw" = {
    source = ../.config/rbw;
    recursive = true;
  };

  # home.file.".config/fish/config.fish" = {
  #   source = ../.config/fish/config.fish;
  # };

  home.file.".config/fish/functions" = {
    source = ../.config/fish/functions;
  };

  programs.ags = {
    enable = true;
    extraPackages = with pkgs; [
      gtksourceview
      webkitgtk
      accountsservice
    ];
  };

  programs.fish = {
    enable = true;
    interactiveShellInit = ''
function fish_greeting -d "what's up, fish?"
    # set_color $fish_color_autosuggestion[1]
    # uname -npsr
    # uptime
    # set_color normal
end

function fish_title
    if [ "$theme_display_virtualenv" = 'no' -o -z "$VIRTUAL_ENV" ]
        printf '(%s) %s' $_ (prompt_pwd)
    else
        printf '(%s) %s' (basename "$VIRTUAL_ENV") (prompt_pwd)
    end
end

function _prompt_whoami -a sep_color -a color -d "Display user@host if on a SSH session"
    if set -q SSH_TTY
        echo -n -s $color (whoami)@(hostnamectl --static) $sep_color '|'
    end
end

function _git_branch_name
    echo (command git symbolic-ref HEAD 2> /dev/null | sed -e 's|^refs/heads/||')
end

function _is_git_dirty
    echo (command git status -s --ignore-submodules=dirty 2> /dev/null)
end

function _git_ahead_count -a remote -a branch_name
    echo (command git log $remote/$branch_name..HEAD 2> /dev/null | \
        grep '^commit' | wc -l | tr -d ' ')
end

function _git_dirty_remotes -a remote_color -a ahead_color
    set current_branch (command git rev-parse --abbrev-ref HEAD 2> /dev/null)
    set current_ref (command git rev-parse HEAD 2> /dev/null)

    for remote in (git remote | grep 'origin\|upstream')

    set -l git_ahead_count (_git_ahead_count $remote $current_branch)

    set remote_branch "refs/remotes/$remote/$current_branch"
    set remote_ref (git for-each-ref --format='%(objectname)' $remote_branch)
    if test "$remote_ref" != \'\'
        if test "$remote_ref" != $current_ref
            if [ $git_ahead_count != 0 ]
                echo -n "$remote_color!"
                echo -n "$ahead_color+$git_ahead_count$normal"
            end
        end
    end
    end
end

function _prompt_git -a gray normal orange red yellow
    test "$theme_display_git" = no; and return
    set -l git_branch (_git_branch_name)
    test -z $git_branch; and return
    if test "$theme_display_git_dirty" = no
        echo -n -s $gray '‹' $yellow $git_branch $gray '› '
        return
    end
    set dirty_remotes (_git_dirty_remotes $red $orange)
    if [ (_is_git_dirty) ]
        echo -n -s $gray '‹' $yellow $git_branch $red '*' $dirty_remotes $gray '› '
    else
        echo -n -s $gray '‹' $yellow $git_branch $red $dirty_remotes $gray '› '
    end
end

function _prompt_pwd
    set_color -o cyan
    printf '%s' (prompt_pwd)
end

function _prompt_status_arrows -a exit_code
    if test $exit_code -ne 0
        set arrow_colors green
    else
        set arrow_colors green
    end

    for arrow_color in $arrow_colors
        set_color $arrow_color
        printf '»'
    end
end

function fish_prompt
    set -l exit_code $status

    set -l gray (set_color 666)
    set -l blue (set_color blue)
    set -l red (set_color red)
    set -l normal (set_color normal)
    set -l yellow (set_color yellow)
    set -l orange (set_color ff9900)
    set -l green (set_color green)

    printf $gray'['

    _prompt_whoami $gray $green

    if test "$theme_display_pwd_on_second_line" != yes
        _prompt_pwd
        printf '%s' $gray
    end

    printf '%s] ⚡️ %0.3fs' $gray (math $CMD_DURATION / 1000)

    if set -q SCORPHISH_GIT_INFO_ON_FIRST_LINE
        set theme_display_git_on_first_line
    end

    if set -q theme_display_git_on_first_line
        _prompt_git $gray $normal $orange $red $yellow
    end

    if test "$theme_display_pwd_on_second_line" = yes
        printf $gray'\n‹'
        _prompt_pwd
        printf $gray'›'
    end

    printf '\n'
    if not set -q theme_display_git_on_first_line
        _prompt_git $gray $normal $orange $red $yellow
    end
    _prompt_status_arrows $exit_code
    printf ' '

    set_color normal
end

function fish_right_prompt
    set -l exit_code $status
    if test $exit_code -ne 0
        set_color red
    else
        set_color green
    end
    printf '%d' $exit_code
    set_color -o 666
    echo '|'
    set_color -o 777
    printf '%s' (date +%H:%M:%S)
    set_color normal
end

### VI MODES
# Emulates vim's cursor shape behavior
# Set the normal and visual mode cursors to a block
set fish_cursor_default block
# Set the insert mode cursor to a line
set fish_cursor_insert line
# Set the replace mode cursor to an underscore
set fish_cursor_replace_one underscore
# The following variable can be used to configure cursor shape in
# visual mode, but due to fish_cursor_default, is redundant here
set fish_cursor_visual block

### BANG BANG FUNCTIONS
function __history_previous_command
    switch (commandline -t)
        case "!"
            commandline -t $history[1]
            commandline -f repaint
        case "*"
            commandline -i !
    end
end

function __history_previous_command_arguments
    switch (commandline -t)
        case "!"
            commandline -t ""
            commandline -f history-token-search-backward
        case "*"
            commandline -i '$'
    end
end

### bindings
function fish_user_key_bindings
    fish_vi_key_bindings
    bind -M insert ! __history_previous_command
    bind -M insert '$' __history_previous_command_arguments
end

# starship init fish | source
'';

  };

  programs.nushell = {
    enable = true;
    shellAliases = {
      # ls = "eza -l";
      la = "ls -la";
      mpf = "mpv --profile=fast";
      mps = "mpv --profile=slow";
      ec = "emacsclient -t";
      ecc = "emacsclient -c";
      # mkdir = "mkdir -p";
      nupd = "update-nix";
      nupg = "upgrade-nix";
      suspend = "systemctl suspend";
      sysuse = "systemctl --user";
      myip = "curl icanhazip.com";
      nixs = "nix search nixpkgs";
      ytd = "yt-dlp -o '~/Videos/%(title)s.%(ext)s'";
    };
    configFile = {
      text = ''
      $env.config = {
        show_banner: false
      }
      '';
    };
    environmentVariables = {
      EDITOR = "\"emacsclient -t\"";
    };
  };

  home.file.".config/dunst" = {
    source = ../.config/dunst;
  };


  programs.atuin.enable = true;

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };

  # programs.qutebrowser.enable = true;
  home.file.".config/qutebrowser/config.py" = {
    source = ../.config/qutebrowser/config.py;
  };

  home.file.".config/qutebrowser/bookmarks" = {
    source = ../.config/qutebrowser/bookmarks;
  };

  home.file.".config/qutebrowser/css" = {
    source = ../.config/qutebrowser/css;
  };

  home.file.".config/qutebrowser/quickmarks" = {
    source = ../.config/qutebrowser/quickmarks;
  };

  home.file.".config/qutebrowser/qsettings" = {
    source = ../.config/qutebrowser/qsettings;
  };

  home.file.".config/imv" = {
    source = ../.config/imv;
    recursive = true;
  };

  home.file.".config/macchina" = {
    source = ../.config/macchina;
    recursive = true;
  };

 #home.file.".config/awesome" = {
 #  source = ../awesome;
 #  recursive = true;
 #};

  home.file.".config/awesome/bling" = {
    source = ../.config/awesome/bling;
    recursive = true;
  };

  home.file.".config/awesome/rubato" = {
    source = ../.config/awesome/rubato;
    recursive = true;
  };

  # home.file.".config/alacritty" = {
  #   source = ../.config/alacritty;
  # };

  home.file.".config/picom.conf" = {
    source = ../.config/picom.conf;
  };

  # home.file.".config/networkmanager-dmenu/config.ini" = {
  #   source = ../networkmanager-dmenu/config.ini;
  # };

  home.file."bin" = {
    source = ../scripts;
    recursive = false;
  };

  home.shellAliases = {
    ls = "eza -l";
    la = "eza -la";
    mpf = "mpv --profile=fast";
    mps = "mpv --profile=slow";
    ec = "emacsclient -t";
    ecc = "emacsclient -c";
    mkdir = "mkdir -pv";
    nupd = "update-nix";
    nupg = "upgrade-nix";
    suspend = "systemctl suspend";
    sysuse = "systemctl --user";
    myip = "curl icanhazip.com";
    nixs = "nix search nixpkgs";
    ytd = "yt-dlp -o \"~/vids/%(title)s.%(ext)s\" $1";
  };

  programs.alacritty = {
    enable = true;
    settings = {
      window = {
        opacity = 1.0;
        dynamic_title = true;
        padding.x = 10;
        padding.y = 10;
        dynamic_padding = true;
      };
      shell.program = "fish";
      font = {
        normal = {
          family = "VictorMono Nerd Font";
          style = "Regular";
        };
        bold = {
          family = "VictorMono Nerd Font";
          style = "Bold";
        };
        italic = {
          family = "VictorMono Nerd Font";
          style = "Italic";
        };
        size = 12.0;
      };
      colors = {
        transparent_background_colors = true;
        draw_bold_text_with_bright_colors = true;
        primary = {
          background = "#282a36";
          foreground = "#eff0eb";
        };
        cursor.cursor = "#97979b";
        selection = {
          text = "#282a36";
          background = "#feffff";
        };
        normal = {
          black = "#282a36";
          red = "#ff5c57";
          green = "#5af78e";
          yellow = "#f3f99d";
          blue = "#57c7ff";
          magenta = "#ff6ac1";
          cyan = "#9aedfe";
          white = "#f1f1f0";
        };
        bright = {
          black = "#686868";
          red = "#ff5c57";
          green = "#5af78e";
          yellow = "#f3f99d";
          blue = "#57c7ff";
          magenta = "#ff6ac1";
          cyan = "#9aedfe";
          white = "#eff0eb";
        };
      };
    };
  };

  programs.starship = {
    enable = true;
    enableNushellIntegration = true;
    enableBashIntegration = true;
    enableFishIntegration = true;
    settings = {
      format = ''
        [\[](fg:#78787e) $username$hostname$directory[\]](fg:#78787e) [$cmd_duration$time$status](italic fg:#78787e)
        $character
      '';
      right_format = ''
        $git_branch$git_status$rust$nix_shell
      '';
      cmd_duration = {
        format = " $duration";
        min_time = 0;
        show_notifications = true;
        min_time_to_notify = 20000;
      };
      time = {
        format = "| $time";
      };
      status = {
        format = "| $status";
      };
      scan_timeout = 10;
      rust = {
        format = "[$symbol($version )]($style)";
      };
      cmake = {
        format = "";
        disabled = true;
      };
      nix_shell = {
        format = "[$symbol$state( \($name\))]($style) ";
        impure_msg = " ";
        pure_msg = " ";
      };
      hostname = {
        format = "@[$ssh_symbol$hostname]($style) > '";
      };
      git_branch = {
        format = "[$symbol$branch(:$remote_branch)]($style) ";
      };
      git_status = {
        format = "[\\[$all_status$ahead_behind\\]]($style) |(dimmed gray) ";
      };
      character = {
        success_symbol = "[󰄾](bold green)";
        error_symbol = "[x](bold red)";
      };
    };
  };

  programs.bash = {
    enable = true;
    bashrcExtra = ''
    # export ENV_EFI_CODE_SECURE=/run/libvirt/nix-ovmf/OVMF_CODE.fd ENV_EFI_VARS_SECURE=/run/libvirt/nix-ovmf/OVMF_VARS.fd
    case "$-" in
      *i*)
      source $(blesh-share)/ble.sh
      ble-face auto_complete="fg=238"
      eval "$(starship init bash)"
      macchina;;
    esac
    # export LESS_TERMCAP_mb=$'\e[1;32m'
    # export LESS_TERMCAP_md=$'\e[1;32m'
    # export LESS_TERMCAP_me=$'\e[0m'
    # export LESS_TERMCAP_se=$'\e[0m'
    # export LESS_TERMCAP_so=$'\e[01;33m'
    # export LESS_TERMCAP_ue=$'\e[0m'
    # export LESS_TERMCAP_us=$'\e[1;4;31m'
    '';
  };

  programs.zsh = {
    enable = true;
    autosuggestion.enable = true;
    enableCompletion = true;
    syntaxHighlighting.enable = true;
    autocd = true;
    dotDir = ".config/zsh";
    shellAliases = {
      ls = "eza -l";
      la = "eza -la";
      mpf = "mpv --profile=fast";
      mps = "mpv --profile=slow";
      ec = "emacsclient -t";
      ecc = "emacsclient -c";
      mkdir = "mkdir -pv";
      nupd = "update-nix";
      nupg = "upgrade-nix";
      suspend = "systemctl suspend";
      sysuse = "systemctl --user";
      myip = "curl icanhazip.com";
      ytd = "yt-dlp -o \"~/vids/%(title)s.%(ext)s\" $1";
    };
    initExtra = ''
      macchina
    '';
  };

  xdg.desktopEntries = {
    mpv-slow = {
      name = "MPV";
      genericName = "Play from MPV but at normal speed";
      exec = "alacritty -e mpv --profile=slow %U";
      terminal = true;
      categories = [ "Application" ];
      mimeType = [ "audio/ogg" "audio/mpeg" "audio/opus" "audio/x-opus+ogg" "audio/x-wav" ];
    };
    imv-rifle = {
      name = "IMV";
      genericName = "Show images in current directory in IMV";
      exec = "/home/chris/bin/rifle-imv %U";
      terminal = false;
      categories = [ "Application" ];
      mimeType = [ "image/gif" "image/jpeg" "image/png" "image/heif" ];
    };
  };

  systemd.user.services = {
    ydotoold = {
      Unit = {
        Description = "An auto-input utility for wayland";
        Documentation = [ "man:ydotool(1)" "man:ydotoold(8)" ];
      };
      
      Service = {
        ExecStart = "/run/current-system/sw/bin/ydotoold --socket-path /tmp/ydotools";
      };

      Install = {
        WantedBy = ["default.target"];
      };
    };

    jellyfin-mpv-shim = {
      Unit = {
        Description = "Play Jellyfin media in mpv";
        After = "graphical-session-pre.target";
      };
      
      Service = {
        ExecStart = "/run/current-system/sw/bin/jellyfin-mpv-shim";
      };

      Install = {
        WantedBy = ["graphical-session.target"];
      };
    };

    # nextcloud-client = {
    #   Unit = {
    #     Description = "Nextcloud Client";
    #     After = [ "graphical-session.target" ];
    #     # PartOf = [ "plasma-workspace.target" ];
    #   };
    #   Service = {
    #     Environment = ["PATH=/etc/profiles/per-user/chris/bin"
    #                    "WAYLAND_DISPLAY=wayland-1"];
    #   };
    # };
  };

}
