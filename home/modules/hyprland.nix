{ config, lib, pkgs, ... }:

let
  laptop = builtins.readFile "/etc/hostname" == "syl\n";
in
{
  home.file.".config/hypr/hyprlock.conf" = {
    text = ''
      background {
         monitor =
         path = /home/chris/pics/wallpapers/nixorange.png   # only png supported for now
         blur_passes = 3 # 0 disables blurring
         blur_size = 7
         noise = 0.0117
         contrast = 0.8916
         brightness = 0.8172
         vibrancy = 0.1696
         vibrancy_darkness = 0.0
      }
      input-field {
         monitor =
         size = 200, 50
         outline_thickness = 3
         dots_size = 0.33 # Scale of input-field height, 0.2 - 0.8
         dots_spacing = 0.15 # Scale of dots' absolute size, 0.0 - 1.0
         dots_center = false
         dots_rounding = -1 # -1 default circle, -2 follow input-field rounding
         outer_color = rgb(151515)
         inner_color = rgb(200, 200, 200)
         font_color = rgb(10, 10, 10)
         fade_on_empty = true
         fade_timeout = 1000 # Milliseconds before fade_on_empty is triggered.
         placeholder_text = <i>Input Password...</i> # Text rendered in the input box when it's empty.
         hide_input = false
         rounding = -1 # -1 means complete rounding (circle/oval)
         check_color = rgb(204, 136, 34)
         fail_color = rgb(204, 34, 34) # if authentication failed, changes outer_color and fail message color
         fail_text = <i>$FAIL <b>($ATTEMPTS)</b></i> # can be set to empty
         fail_transition = 300 # transition time in ms between normal outer_color and fail_color
         capslock_color = -1
         numlock_color = -1
         bothlock_color = -1 # when both locks are active. -1 means don't change outer color (same for above)
         invert_numlock = false # change color if numlock is off
         swap_font_color = false # see below
        
         position = 0, -20
         halign = center
         valign = center
      }
    '';
  };
  home.file.".config/hypr/hypridle.conf" = {
    text = ''
    general {
      lock_cmd = pidof hyprlock || hyprlock       # avoid starting multiple hyprlock instances.
      before_sleep_cmd = loginctl lock-session    # lock before suspend.
      after_sleep_cmd = hyprctl dispatch dpms on  # to avoid having to press a key twice to turn on the display.
     }

     listener {
       timeout = 150                                # 2.5min.
       on-timeout = brightnessctl -s set 10         # set monitor backlight to minimum, avoid 0 on OLED monitor.
       on-resume = brightnessctl -r                 # monitor backlight restore.
     }

     listener {
       timeout = 300                                 # 5min
       on-timeout = loginctl lock-session            # lock screen when timeout has passed
     }

     listener {
       timeout = 330                                 # 5.5min
       on-timeout = hyprctl dispatch dpms off        # screen off when timeout has passed
       on-resume = hyprctl dispatch dpms on          # screen on when activity is detected after timeout has fired.
     }

     listener {
       timeout = 1800                                # 30min
       on-timeout = systemctl suspend                # suspend pc
     }
    '';
  };
  home.file.".config/hypr/hyprpaper.conf" = {
    source = if laptop then /home/chris/.dotfiles/.config/hypr/hyprpaper.conf else /home/chris/.dotfiles/.config/hypr/hyprpaper.conf;
  };

  wayland.windowManager.hyprland = {
    enable = true;
    systemd.variables = ["--all"];
    settings = {
      monitor = [
        "eDP-1,2256x1504@60,0x0,1.566667"
        "HDMI-A-1,preferred,0x0,1"
        "DP-1,preferred,1080x0,1.5"
        "DP-2,preferred,3640x0,1"
        "HDMI-A-1,transform,1"
      ];
      workspace = [
        "1,name:code,monitor:eDP-1,1,default:true,persistent:true"
        "2,name:browse,monitor:DP-1,default:true,persistent:true"
        "9,name:monitor,monitor:DP-2,gapsout:0,rounding:false,default:true,persistent:true"
        "1,monitor:DP-1,1,default:true,persistent:true"
        "special,monitor:DP-1,1,default:true"
      ];
      input = {
        repeat_rate = 140;
        repeat_delay = 180;
        natural_scroll = 0;

        sensitivity = 0.75;
        accel_profile = "adaptive";

        follow_mouse = 2;
        float_switch_override_focus = 0;
        
        touchpad = {
          natural_scroll = 1;
          clickfinger_behavior = 1;
        };
      };

     #debug = {
     #  enable_stdout_logs = false;
     #};

      general = {
        gaps_in = 15;
        gaps_out = 20;
        border_size = 0;
        "col.active_border" = "0x66ee1111";
        "col.inactive_border" = "0x66333333";

        #damage_tracking=full # leave it on full unless you hate your GPU and want to make it suffer
        layout = "master";
        no_cursor_warps = false;
      };

      gestures = {
        workspace_swipe = true;
        workspace_swipe_fingers = 3;
      };

      decoration = {
        rounding = 18;
        # multisample_edges=true
        active_opacity = 0.97;
        inactive_opacity = 0.97;
        fullscreen_opacity = 1.0;

        blur = {
          enabled = true;
          size = 10; # minimum 1;
          passes = 3; # minimum 1, more passes = more resource intensive.;
          new_optimizations = true;
          ignore_opacity = false;
          special = false;
        };

        drop_shadow = true;
        shadow_range = 15;
        shadow_offset = "5 5";
        shadow_scale = 0.99;
        shadow_render_power = 1;
        "col.shadow" = "rgba(000000cc)";
        dim_inactive = false;
        dim_strength = 0.2;
        dim_special = 0;
      };

      bezier = "snapslide,0.07,0.66,0.04,1.02";

      animations = {
        enabled = 1;
        animation = [
          "windows,1,2,snapslide"
          "fadeIn,1,2,snapslide"
          "workspaces,1,2,snapslide"
          "specialWorkspace,1,2,snapslide,slidevert"
        ];
      };

      dwindle = {
        pseudotile = 0; # enable pseudotiling on dwindle
      };

      master = {
        special_scale_factor = 0.8;
        mfact = 0.65;
        new_is_master = false;
        new_on_top = false;
      };

      # misc:disable_autoreload = true
      misc = {
        focus_on_activate  =  true;
        mouse_move_enables_dpms  =  true;
        key_press_enables_dpms  =  true;
        disable_hyprland_logo  =  true;
      };

      # example window rules
      # for windows named/classed as abc and xyz
      windowrule = [
        "opaque,ff"
        # if !laptop then "workspace 1,mpv"
        "float,dolphin"
        "float,nm-tray"
        "size 60% 60%,dolphin"
        "float,mpv"
        "size ${if laptop then "90% 76%" else "85% 85%"},mpv"
        "${if laptop then "workspace 1,idk #dummy rule" else "workspace 9,mpv"}"
        "center,mpv"
        "opaque,mpv"
        "float,pulsemixer"
        "workspace 2,ff"
        "workspace 1,emacs"
        "float,btop"
        "size 70% 70%,btop"
        "center,btop"
        "float,lumina"
        "size 80% 80%,lumina"
        "center,lumina"
        "opaque,lumina"
        "workspace ${if laptop then "1" else "3"},title:presentation-window"
      ];

      # example binds
      bind = [
        "SUPER,RETURN,exec,alacritty"
        "SUPER,C,killactive,"
        "SUPERSHIFT,Q,exit,"
        "SUPERSHIFT,D,exec,dolphin"
        "SUPERSHIFT,F,togglefloating,"
        "SUPER,w,exec,/home/chris/bin/window.sh"
        "SUPER,E,exec,/home/chris/bin/emacslof"
        "SUPER,d,exec,emacsclient -c -e '(dired-jump)'"
        "SUPER,v,exec,cliphist list | rofi -p '󱃔 ' -dmenu -theme ~/.config/rofi/launchers-git/laptop-rbw-wayland.rasi | cliphist decode | wl-copy"
        "SUPER,B,exec,/home/chris/bin/fflof"
        "SUPER,A,exec,alacritty --class pulsemixer -e pulsemixer"
        "SUPERCTRL,i,exec,alacritty --class btop -e btop"
        ",Print,exec,screenshot"
        "SUPERSHIFT,mouse_down,exec,zoomin in"
        "SUPERSHIFT,mouse_up,exec,zoomin out"

        "SUPER,P,exec,/home/chris/bin/rbw.sh"
        "SUPER,M,fullscreen,1"
        "SUPER,F,fullscreen,0"

        "SUPER,h,movefocus,l"
        "SUPER,l,movefocus,r"
        "SUPER,k,layoutmsg,cycleprev"
        "SUPER,j,layoutmsg,cyclenext"
        "SUPERSHIFT,h,movewindow,l"
        "SUPERSHIFT,l,movewindow,r"
        "SUPERSHIFTALT,h,layoutmsg,addmaster"
        "SUPERSHIFTALT,l,layoutmsg,addmaster"
        "SUPERSHIFT,k,layoutmsg,swapprev"
        "SUPERSHIFT,j,layoutmsg,swapnext"
        "SUPERCTRL,l,splitratio,+0.05"
        "SUPERCTRL,h,splitratio,-0.05"
        "SUPERSHIFT,c,centerwindow"
        "ALTCTRL,l,moveintogroup,right"
        "ALTCTRL,h,moveintogroup,left"
        "ALTCTRL,k,moveintogroup,up"
        "ALTCTRL,j,moveintogroup,down"
        "SUPERALT,g,togglegroup"

        "SUPER,1,workspace,1"
        "SUPER,2,workspace,2"
        "SUPER,3,workspace,3"
        "SUPER,4,workspace,4"
        "SUPER,5,workspace,5"
        "SUPER,6,workspace,6"
        "SUPER,7,workspace,7"
        "SUPER,8,workspace,8"
        "SUPER,9,workspace,9"
        "SUPER,0,workspace,10"

        "SUPERALT,l,workspace,m+1"
        "SUPERALT,h,workspace,m-1"

        "SUPERSHIFT,1,movetoworkspace,1"
        "SUPERSHIFT,2,movetoworkspace,2"
        "SUPERSHIFT,3,movetoworkspace,3"
        "SUPERSHIFT,4,movetoworkspace,4"
        "SUPERSHIFT,5,movetoworkspace,5"
        "SUPERSHIFT,6,movetoworkspace,6"
        "SUPERSHIFT,7,movetoworkspace,7"
        "SUPERSHIFT,8,movetoworkspace,8"
        "SUPERSHIFT,9,movetoworkspace,9"
        "SUPERSHIFT,0,movetoworkspace,10"

        "SUPER,o,movewindow,mon:l"
        "SUPER,y,togglespecialworkspace"
        "SUPER,i,movewindow,left"
        "SUPERSHIFT,p,pin"
        "SUPER,n,movetoworkspace,special"
        "SUPERALT,a,movetoworkspace,special:agenda"
        "SUPER,g,togglespecialworkspace,agenda"

        "SUPERSHIFT,g,exec,ags --toggle-window ${if laptop then "bar0" else "bar2"}"
        "SUPERALT,n,exec,eww update rightside=true"
      ];

      binde = [
        ", XF86AudioMute, exec, pamixer -t"
        ", XF86AudioRaiseVolume, exec, /home/chris/bin/volup"
        ", XF86AudioLowerVolume, exec, /home/chris/bin/voldown"
        ",F1, exec, pamixer -t"
        ",F3, exec, /home/chris/bin/volup"
        ",F2, exec, /home/chris/bin/voldown"
        ", XF86MonBrightnessUp, exec, brightnessctl s +10%"
        ", XF86MonBrightnessDown, exec, brightnessctl s 10%-"
      ];

      bindr = [
        "SUPER,Super_L,exec,/home/chris/bin/launcher.sh"
        "ALT,Alt_R,exec,/home/chris/bin/window.sh"
      ];

      bindm = [
        "SUPER,mouse:272,movewindow"
        "SUPER,mouse:273,resizewindow"
      ];

      # Blur waybar
      # blurls = "gtk-layer-shell";
      # blurls=notifications
      
      exec-once = [
        "kwalletd5"
        # "eww daemon"
        "swww-daemon --format xrgb"
        # "eww open ${if laptop then "bar0" else "bar1"}"
        # "dunst"
        "ags"
        "rbw-agent"
        "hyprctl dispatch --batch 'splitratio 1; splitration -0.35'"
        "dbus-update-activation-environment --systemd --all"
        "batmon.lisp"
        "/usr/lib/kdeconnectd"
        "systemctl --user stop jellyfin-mpv-shim"
        "systemctl --user stop emacs"
        "systemctl --user stop nextcloud-client"
        "emacs --daemon"
        "jellyfin-mpv-shim"
        "nextcloud"
        "kdeconnect-indicator"
        "wl-paste --watch cliphist store"
        "hyprctl setcursor phinger-cursors-light 24"
        "hypridle"
        "nm-tray"
        "sleep 3 && swww img /home/chris/pics/wallpapers/nixorange.jpeg -t grow --transition-bezier .14,0,.14,.99"
      ];
    };
  };
}
