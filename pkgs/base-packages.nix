{ pkgs, config, ... }:

{
  environment.systemPackages = with pkgs; [
    vim
    wget
    killall
    gnumake
    smartmontools
    git
    tmux
    dutree
    cachix
    unzip
    ollama
    ledger
    recutils
    guile
    guile-sqlite3
    guile-config
    guile-sjson
    guile-json
    guile-lib
    scsh
    radicle-cli
    # unrar
    p7zip
    zip
    gzip
    usbutils
    binutils
    podman-compose
    eza
    nushell
    iperf
    # img2pdf
    yt-dlp
    bat
    rsync
    jq
    ripgrep
    fd
    socat
    ffmpeg_5-full
    imagemagick
    libheif
    trash-cli
    htop
    btop
    bc 
    sysstat
    procs
    pandoc
    samba
    blesh
    # OVMFFull
    quickemu
    macchina
    neofetch
    fwupd
    ts
    du-dust
    sbcl

    clojure
    clojure-lsp
    clj-kondo
    leiningen
  ];
}

