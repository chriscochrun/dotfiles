{
  stdenv,
  lib,
  fetchFromGitHub,
  qtbase,
  qtx11extras,
  qttools,
  kglobalaccel,
  kinit,
  kwin,
  kdecoration,
  kcrash,
  kio,
  knotifications,
  kguiaddons,
  kcoreaddons,
  # systemsettings,
  cmake,
  extra-cmake-modules,
  wrapQtAppsHook,
  libepoxy,
  kdelibs4support,
  libXdmcp
}:

stdenv.mkDerivation rec {
  pname = "RoundedSBE";
  version = "0.1.0";

  src = fetchFromGitHub {
    owner = "a-parhom";
    repo = "RoundedSBE";
    rev = "ba60e3c84ed0bab18d369435e7d8cdd1e33fd6b2";
    sha256 = "sha256-soQdRykn0WucfQkiEdMZTVmvxNu1i0siS3f8LAUMKMQ=";
  };

  # dontWrapQtApps = true;

  buildInputs = [
    qtbase
    qtx11extras
    qttools
    kglobalaccel
    kdelibs4support
    kcrash
    kinit
    kwin
    kdecoration
    kio
    knotifications
    kguiaddons
    kcoreaddons
    # systemsettings
    libepoxy
    libXdmcp
  ];

  nativeBuildInputs = [
    cmake
    extra-cmake-modules
    wrapQtAppsHook
  ];

  # postConfigure = ''
  #   substituteInPlace cmake_install.cmake \
  #     --replace "${kdelibs4support}" "$out"

  # '';

  cmakeFlags = [
    "-DCMAKE_INSTALL_PREFIX=$out"
    "-DCMAKE_BUILD_TYPE=Release"
    "-DBUILD_TESTING=OFF"
    "-DKDE_INSTALL_USE_QT_SYS_PATHS=ON"
  ];

  meta = with lib; {
    name = "RoundedSBE";
    description = "Round corners and outline effect for Sierra Breeze Enhanced.";
    homepage = "https://github.com/a-parhom/RoundedSBE";
    license = licenses.gpl3;
    maintainers = [ "chriscochrun" ];
    platforms = platforms.all;
  };
}
