{
  stdenv,
  lib,
  fetchFromGitHub,
  qtbase,
  qtx11extras,
  qttools,
  kglobalaccel,
  kinit,
  kwin,
  kdecoration,
  kcrash,
  kio,
  knotifications,
  kguiaddons,
  kcoreaddons,
  # systemsettings,
  cmake,
  qt5base,
  extra-cmake-modules,
  wrapQtAppsHook,
  libepoxy,
  kdelibs4support,
  libXdmcp
}:

stdenv.mkDerivation rec {
  pname = "LightlyShaders";
  version = "2.0.0";

  src = fetchFromGitHub {
    owner = "a-parhom";
    repo = "LightlyShaders";
    rev = "v2.0";
    sha256 = "sha256-p94VwzGHB/4qxthvG507JEXx9j2MZDo6yyElq3XQxYc=";
  };

  # dontWrapQtApps = true;

  buildInputs = [
    qtbase
    qtx11extras
    qttools
    kglobalaccel
    kdelibs4support
    kcrash
    kinit
    kwin
    kdecoration
    kio
    knotifications
    kguiaddons
    kcoreaddons
    # systemsettings
    libepoxy
    libXdmcp
  ];

  nativeBuildInputs = [
    cmake
    extra-cmake-modules
    wrapQtAppsHook
  ];

  postConfigure = ''
    substituteInPlace cmake_install.cmake \
      --replace "${kdelibs4support}" "$out"

  '';

  meta = with lib; {
    name = "LightlyShaders";
    description = "Round corners and outline effect for KWin.";
    homepage = "https://github.com/a-parhom/LightlyShaders";
    license = licenses.gpl3;
    maintainers = [ "chriscochrun" ];
    platforms = platforms.all;
  };
}
