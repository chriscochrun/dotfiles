{
  stdenv,
  lib,
  fetchFromGitHub,
  qtbase,
  qtx11extras,
  qttools,
  kglobalaccel,
  kinit,
  kwin,
  kio,
  kguiaddons,
  kcoreaddons,
  systemsettings,
  plasma-framework,
  cmake,
  extra-cmake-modules,
  libepoxy,
  libXdmcp
}:

stdenv.mkDerivation rec {
  pname = "LightlyShaders";
  version = "0.0.2";

  src = fetchFromGitHub {
    owner = "a-parhom";
    repo = "LightlyShaders";
    rev = "d2a643b0c56d50d2ac1822e04c7b83d72de4213e";
    sha256 = "MJk2pXRmyZDZX7KeZBHNKi0WQcQwqTM1nZWTWSxnvfc=";
  };

  dontWrapQtApps = true;

  buildInputs = [
    qtbase
    qtx11extras
    qttools
    kglobalaccel
    kinit
    kwin
    kio
    kguiaddons
    kcoreaddons
    systemsettings
    plasma-framework
  ];

  nativeBuildInputs = [
    stdenv
    cmake
    extra-cmake-modules
    libepoxy
    libXdmcp
  ];

  preConfigure = ''
    local modulepath=$(kf5-config --install module)
    local datapath=$(kf5-config --install data)
    local servicepath=$(kf5-config --install services)
    substituteInPlace CMakeLists.txt \
      --replace "\''${MODULEPATH}" "$out/''${modulepath#/nix/store/*/}" \
      --replace "\''${DATAPATH}"   "$out/''${datapath#/nix/store/*/}" \
      --replace "\''${SERVICES_INSTALL_DIR}"   "$out/''${servicepath#/nix/store/*/}"
  '';

  meta = with lib; {
    name = "LightlyShaders";
    description = "Round corners and outline effect for KWin.";
    homepage = "https://github.com/a-parhom/LightlyShaders";
    license = licenses.gpl3;
    maintainers = [ "chriscochrun" ];
    platforms = platforms.all;
  };
}
