{ pkgs, lib, config, ... }:

with lib;
{
  environment.systemPackages = with pkgs; [
    discover
    lightly-qt
    pinentry
    pinentry-qt
    caffeine-ng
    hunspell
    hunspellDicts.en_US-large
    transmission
    openssh
    openssl
    cyrus-sasl-xoauth2
    ark
    pantalaimon
    ifuse
    dash
    dolphin
    guix
    freecad
    picard
    brightnessctl
    mpc-cli
    dunst
    dvdbackup
    bottom
    ungoogled-chromium
    qutebrowser
    brave
    ttyper
    kget
    audacity
    krename
    kwallet-pam
    nm-tray
    # nyxt
    sbcl
    openjdk
    qmk
    aria2
    sassc
    # pantalaimon
    plasma5Packages.kwallet
    # sierra-breeze-enhanced
    libimobiledevice
    # textgen-amd
    # bottles
    # jitsi-meet-electron
    imv
    feh
    tagutil
    # python310Packages.mutagen
    python310Packages.audiotools
    (mpv.override {scripts = with pkgs.mpvScripts; [ mpris quality-menu sponsorblock ];})
    # haruna
    ani-cli
    # mov-cli
    nerdfonts
    plasma-browser-integration
    alacritty
    # libsForQt5.bismuth
    libnotify
    rofi-wayland
    #anyrun
    wf-recorder
    wofi
    waybar
    tridactyl-native
    eww
    scc
    tokei
    wlrctl
    wl-clipboard
    hyprpaper
    hyprlock
    hypridle
    aha
    glxinfo
    vulkan-tools
    wayland-utils
    nextcloud-client
    qt5.qtbase
    qt5.qtwayland
    qt6.qtwayland
    swww
    grim
    slurp
    # mkchromecast
    plocate
    # librepresenter.libre-presenter
    papirus-icon-theme
    phinger-cursors
    plasma-hud
    kde-cli-tools
    gzip 
    qrencode
    # brave
    # scribus
    # darktable
    # qutebrowser
    virt-manager
    virt-viewer
    # firefox
    # kate
    kdialog
    plasma5Packages.khotkeys
    # openlp
    inkscape
    libreoffice-fresh
    vlc
    # neochat
    haskellPackages.greenclip
    pulsemixer
    any-nix-shell
    wtype
    xdotool
    # ydotool
    wmctrl
    xcape
    xclip
    # maim
    unclutter-xfixes
    bluez-tools
    networkmanager_dmenu
    plasma5Packages.qt5ct
    xdg-desktop-portal-kde
    lxappearance
    spotdl
    # (kdenlive.override {
    #     mlt = mlt.override {
    #       ffmpeg = ffmpeg-full;
    #     };
    #   })
    kdenlive
    # olive-editor
    # davinci-resolve
    natron
    glaxnimate
    mediainfo
    libmediainfo
    pqiv
    plasma5Packages.audiotube
    # natron
    # digikam
    rubberband
    #texlive
    wlroots
    pamixer
    playerctl
    jellyfin-mpv-shim
    # pfetch
    macchina
    gimp
    krita
    powertop
    element-desktop-wayland
    scrcpy
    python3
    # hyprland
    # (callPackage ./ydotool {  })
    # (libsForQt5.callPackage /home/chris/dev/LightlyShaders {}) # LightlyShaders
    # (libsForQt5.callPackage /home/chris/.dotfiles/RoundedSBE {}) 
    # (libsForQt5.callPackage /home/chris/dev/church-presenter {}) # librepresenter
    # (libsForQt5.callPackage /home/chris/dev/tyler {}) # librepresenter
    nix-index
    sqlite
    fennel
    blesh
    plasma5Packages.plasma-sdk
    ardour
    qpwgraph
    zam-plugins
    tap-plugins
    lsp-plugins
    ladspaPlugins
    calf
    carla
    esphome
    esptool
    wireguard-tools
  ];
}
