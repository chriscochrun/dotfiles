{ pkgs, lib, config, ... }:

with lib;

let
  port = 8999;

in
{
  services.audiobookshelf = {
    enable = true;
    host = "0.0.0.0";
    port = port;
  };

  services.caddy.virtualHosts = {
      "abs.cochrun.xyz".extraConfig = ''
        reverse_proxy localhost:8999
      '';
  };
}
  
