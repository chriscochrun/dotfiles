{ pkgs, lib, config, stable, ... }:

with lib;
{

  # nix = {
  #   package = pkgs.nixVersions.git;
  # };


  # This adds hyprland cache to cachix and relaxes sandbox
  nix.settings = {
    substituters = ["https://hyprland.cachix.org"];
    trusted-public-keys = ["hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="];
    sandbox = "relaxed";
  };
  
  # Use the systemd-boot EFI boot loader.
  boot = {
    kernelPackages = pkgs.linuxPackages_zen;
    kernelParams = [ "mem_sleep_default=deep" "nvme.noacpi=1"];
    # initrd.kernelModules = [ "amdgpu" ];
  };

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;

  boot.supportedFilesystems = [ "ntfs" ];
  services.xserver = {
    enable = true;
    autorun = false;
    windowManager.awesome = {
      enable = true;
      package = pkgs.awesome;
    };
    desktopManager.plasma5 = {
      enable = true;
      runUsingSystemd = true;
    };
    # desktopManager.gnome.enable = true;
  };

  programs.ssh.startAgent = true;
  security.pam.services.kwallet = {
    name = "kwallet";
    enableKwallet = true;
  };

  systemd.extraConfig = ''
    DefaultTimeoutStopSec=10s
  '';

  # stylix = {
  #   enable = true;
  #   polarity = "dark";
  #   base16Scheme = {
  #     base00 = "282a36";
  #     base01 = "34353e";
  #     base02 = "43454f";
  #     base03 = "78787e";
  #     base04 = "a5a5a9";
  #     base05 = "e2e4e5";
  #     base06 = "eff0eb";
  #     base07 = "f1f1f0";
  #     base08 = "ff5c57";
  #     base09 = "ff9f43";
  #     base0A = "f3f99d";
  #     base0B = "5af78e";
  #     base0C = "9aedfe";
  #     base0D = "57c7ff";
  #     base0E = "ff6ac1";
  #     base0F = "b2643c";
  #   };
  #   image = "base0D";
  #   # targets.tofi.enable = false;
  # };

  fonts.packages = with pkgs; [
                            nerdfonts
                            noto-fonts
                            noto-fonts-cjk
                            noto-fonts-emoji
                            liberation_ttf
                            mplus-outline-fonts.githubRelease
                            dina-font
                            proggyfonts
                            ];

  hardware = {
    keyboard.qmk.enable = true;
  };

  security.pam.services = {
    sddm.enableKwallet = true;
    greetd.enableKwallet = true;
    swaylock = {};
  };

  services.greetd = {
    enable = true;
    settings = rec {
      initial_session = {
        command = "dbus-run-session Hyprland";
        # command = "dbus-run-session startplasma-wayland";
        user = "chris";
      };
      default_session = {
        command = "${pkgs.greetd.tuigreet}/bin/tuigreet --time -r --window-padding 2 --cmd 'dbus-run-session Hyprland' -s /etc/greetd/environments";
        user = "greeter";
      };
      plasma_session = {
        command = "dbus-run-session startplasma-wayland";
        user = "chris";
      };
      hyprland_session = {
        command = "Hyprland";
        user = "chris";
      };
    };
  };

  environment.etc."greetd/environments".text = ''
    hyprland
    dbus-run-session startplasma-wayland
    fish
    bash
  '';

  qt = {
    platformTheme = "qt5ct";
    enable = true;
  };

  # environment.etc."xdg/user-dirs.defaults".text = ''
  #   DESKTOP=
  #   DOWNLOAD=dls
  #   TEMPLATES=
  #   PUBLICSHARE=
  #   DOCUMENTS=docs
  #   MUSIC=music
  #   PICTURES=pics
  #   VIDEOS=vids
  # '';

  xdg.portal = {
    enable = true;
    extraPortals = [ pkgs.xdg-desktop-portal-kde ];
  };

  programs.hyprland = {
    enable = true;
    xwayland.enable = true;
    # legacyRenderer = true;
    # package = stable.hyprland;
    # portalPackage = pkgs.xdg-desktop-portal-kde;
    # xwayland.hidpi = true;
  };

  services.avahi = {
    enable = true;
    nssmdns4 = true;
  };

  # Enable sound.
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
    wireplumber.enable = true;
  };

  # Needed for some pipewire progs and other gtk apps
  programs.dconf.enable = true;

  environment.variables = {
      EDITOR = "emacsclient -t -a";
      VISUAL = "emacsclient -c -a";
      QT_LOGGING_RULES = "js=true; kwin_*.debug=true";
      # GTK_USE_PORTAL = "1";
      # Fixing Qt apps in other environments
      # Thought I needed this but apparently they are working fine without it.
      # QT_XCB_GL_INTEGRATION = "xcb_egl";
      # QT_QPA_PLATFORM_PLUGIN_PATH = "${pkgs.qt5.qtbase.bin}/lib/qt-${pkgs.qt5.qtbase.qtCompatVersion}/plugins/platforms";
      NIXOS_OZONE_WL = "1";
      _JAVA_AWT_WM_NONEREPARENTING = "1";
      # DISABLE_QT5_COMPAT = "0";
      GDK_BACKEND = "wayland";
      ANKI_WAYLAND = "1";
      WLR_DRM_NO_ATOMIC = "1";
      QT_AUTO_SCREEN_SCALE_FACTOR = "1";
      QT_QPA_PLATFORM = "wayland";
      # QT_WAYLAND_DISABLE_WINDOWDECORATION = "1";
      # QT_QPA_PLATFORMTHEME = "qt5ct";
      MOZ_ENABLE_WAYLAND = "1";
      MOZ_DBUS_REMOTE = "1";
      WLR_BACKEND = "vulkan";
      WLR_RENDERER = "vulkan";
      XDG_SESSION_TYPE = "wayland";
      # Remember that this causes issues in some games use "unset SDL_VIDEODRIVER &&"
      SDL_VIDEODRIVER = "wayland";
      CLUTTER_BACKEND = "wayland";
      WLR_NO_HARDWARE_CURSORS = "1";
  };

  programs.partition-manager.enable = true;
  programs.kdeconnect.enable = true;

  # Turn on flatpak
  services.flatpak.enable = true;

  # Configure keymap in X11
  services.xserver.xkb.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.gutenprint pkgs.gutenprintBin ]; 

  services.usbmuxd.enable = true;
  services.fstrim.enable = true;

  # services.pantalaimon-headless = {
  #   instances = {
  #     tfc = {
  #       homeserver = "https://matrix.tfcconnection.org";
  #       # listenAddress = "localhost";
  #       listenPort = 8008;
  #       ssl = true;
  #       # extraSettings = {
  #       #   IgnoreVerification = true;
  #       # }
  #     };
  #   };
  # };

  #######################
  # VIRTUALIZATION
  #######################

  # virtualisation.waydroid.enable = true;
  virtualisation.podman.enable = true;
  # virtualisation.docker.enable = true;
  virtualisation.libvirtd = {
    enable = true;
    qemu = {
      runAsRoot = false;
      ovmf = {
        enable = true;
        # packages = [
        #   pkgs.OVMFFull.fd
        #   pkgs.pkgsCross.aarch64-multiplatform.OVMF.fd
        # ];
      };
      swtpm.enable = true;
    };
  };
  services.samba-wsdd.enable = true;
  services.samba = {
    enable = true;
    extraConfig = ''
      workgroup = WORKGROUP                                     
      server string = smbnix
      netbios name = smbnix
      security = user 
      #use sendfile = yes
      #max protocol = smb2
      # note: localhost is the ipv6 localhost ::1
      hosts allow = 192.168.0. 127.0.0.1 localhost
      hosts deny = 0.0.0.0/0
      guest account = nobody
      map to guest = bad user
  '';
    shares = {
      public = {
        path = "/home/chris";
        "read only" = false;
        browsable = true;
        "guest ok" = true;
        comment = "Share";
      };
    };
  };

  # Android Tools
  programs.adb.enable = true;
  services.udev.packages = [ pkgs.android-udev-rules ];

  services.esphome = {
    enable = true;
  };
}
