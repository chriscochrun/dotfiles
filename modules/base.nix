{ pkgs, lib, config, ... }:

with lib;
{
  imports = [ ./cachix.nix ];

  nix = {
   extraOptions = "experimental-features = nix-command flakes";
   gc = {
     automatic = true;
     dates = "weekly";
     options = "--delete-older-than 20d";
   };
   settings = {
     auto-optimise-store = true;
   };
  };
  # nixpkgs.config.allowUnFree = true;
  # pkgs.config.allowUnfree = true;

  system.autoUpgrade = {
    enable = true;
    dates = "02:00";
    allowReboot = false;
    flake = "${config.users.users.chris.home}/conf";
    flags = [ "--update-input" "nixpkgs" "--commit-lock-file" ];
  };

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "America/Chicago";

  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # Set default shell to be dash for speed
  # Apparently this is bad because a lot of nix relies on bash
  # environment.binsh = "${pkgs.dash}/bin/dash";
  environment.homeBinInPath = true;
  programs.fish.enable = true;
  programs.zsh.enable = true;

}
