{ config, lib, pkgs, ... }:

with lib;
{
  services.matrix-synapse = {
    enable = true;
    withJemalloc = true;
    settings = {
      enable_metrics = true;
      enable_registration = true;
      dynamic_thumbnails = true;
      max_upload_size = "100M";
      public_baseurl = "https://matrix.cochrun.xyz/";
      server_name = "cochrun.xyz";
    };
  };

  services.caddy.virtualHosts = {
      "matrix.cochrun.xyz".extraConfig = ''
        reverse_proxy localhost:8008
      '';
      # "matrix.cochrun.xyz/_matrix".extraConfig = ''
      #   reverse_proxy localhost:6167
      # '';
  };

  services.matrix-conduit = {
    enable = false;
    settings = {
      global = {
        allow_registration = true;
        server_name = "cochrun.xyz";
      };
    };
  };
}
