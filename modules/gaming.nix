{ pkgs, lib, config, ... }:

with lib;
{
  hardware.opengl = {
    driSupport = true;
    driSupport32Bit = true;
  };
  programs.steam = {
    enable = true;
    gamescopeSession.enable = true;
  };
  programs.gamemode.enable = true;
  environment.systemPackages = with pkgs; [
    airshipper
    lutris
    heroic
    protonup-qt
    winePackages.waylandFull
    winetricks
    # gamescope
    # cemu
    # yuzu
    mangohud
    openmw
    dwarfs
    fuse-overlayfs
    steam-run
  ];
}
