{ pkgs, lib, config, ... }:
with lib;
{
  services.emacs = {
    enable = true;
    package = with pkgs; ((emacsPackagesFor pkgs.emacs-pgtk).emacsWithPackages 
      (epkgs: with epkgs; [
        vterm
        melpaPackages.magit
        pdf-tools
        consult-eglot
        org
        bluetooth
        melpaPackages.org-roam
        melpaPackages.org-ql
        melpaPackages.dired-sidebar
        melpaPackages.lab
        posframe
        vertico-posframe
        chatgpt-shell
        denote
        # geiser-guile
        # flymake-guile
        # flycheck-guile
        denote-refs
        command-log-mode
        all-the-icons
        doom-modeline
        doom-themes
        ligature
        rec-mode
        melpaPackages.mini-echo
        llm
        rainbow-delimiters
        smartparens
        paredit
        aggressive-indent
        adaptive-wrap
        which-key
        exec-path-from-shell
        no-littering
        languagetool
        rustic
        melpaPackages.slint-mode
        flycheck-clj-kondo
        clojure-mode
        cider
        melpaPackages.evil
        melpaPackages.evil-collection
        melpaPackages.ellama
        general
        evil-escape
        evil-surround
        evil-org
        org-super-agenda
        websocket
        org-roam-ui
        org-present
        org-modern
        # (org-re-reveal.overrideAttrs (o: {
        #   src = pkgs.fetchgit {
        #     url = "https://gitlab.com/oer/org-re-reveal";
        #     rev = "7c39d15b841c7a8d197a24c89e5fef5d54e271aa";
        #     sha256 = "/1eXxIY8SqLLC10ekGs7G3o7U7MIA01mtsl2C6lo7us=";
        #   };
        # }))
        # org-re-reveal
        melpaPackages.org-re-reveal
        # melpaPackages.org-re-reveal-ref
        # melpaPackages.org-re-reveal-citeproc
        org-web-tools
        org-transclusion
        # ox-reveal
        ox-hugo
        # oer-reveal
        unicode-fonts
        emojify
        undo-tree
        visual-fill-column
        toc-org
        pulsar
        vertico
        melpaPackages.consult
        melpaPackages.marginalia
        all-the-icons-completion
        melpaPackages.embark
        melpaPackages.embark-consult
        corfu
        jinx
        eat
        kind-icon
        melpaPackages.orderless
        melpaPackages.cape
        devdocs
        nano-theme
        nano-modeline
        nano-agenda
        yasnippet
        wgrep
        melpaPackages.tempel
        melpaPackages.tempel-collection
        projectile
        simple-httpd
        avy
        evil-avy
        ace-link
        ace-window
        helpful
        format-all
        web-mode
        php-mode
        lua-mode
        nix-mode
        nix-update
        cmake-mode
        fennel-mode
        yaml-mode
        typescript-mode
        docker
        # docker-tramp
        fish-mode
        markdown-mode
        qml-mode
        csv-mode
        restclient
        ob-restclient
        dart-mode
        flutter
        hover
        direnv
        all-the-icons-dired
        dired-single
        dired-rainbow
        diredfl
        dired-rsync
        fd-dired
        ledger-mode
        org-msg
        calfw
        calfw-org
        calfw-ical
        org-caldav
        org-wild-notifier
        sly
        nov
        elfeed
        elfeed-org
        elfeed-protocol
        bongo
        empv
        emms
        transmission
        hass
        pass
        password-store
        password-store-otp
        plz
        elpaPackages.ement
        mastodon
        qrencode
        just-mode
        justl
        gcmh
        mu4e
        use-package
        esh-autosuggest
        melpaPackages.org-ai
        melpaPackages.gptel
        pkgs.ispell
        pkgs.mu
        pkgs.openjdk
        pkgs.languagetool
        pkgs.emacs-all-the-icons-fonts
      ]));
    defaultEditor = true;
  };
}
