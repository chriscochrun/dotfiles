;; This file is my laptop OS
;; 
;; Author: Chris Cochrun
;; Email: chris@cochrun.xyz
;; 
;; License: GPLv3
;; 

(define-module (syl)
  #:use-module (base)
  #:use-module (gnu)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services dbus)
  #:use-module (gnu system)
  #:use-module (gnu system setuid)
  #:use-module (gnu system nss)
  #:use-module (gnu system shadow)
  #:use-module (gnu packages android)
  #:use-module (rosenthal packages wm)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd))


(operating-system
  (inherit base-operating-system)
  (host-name "syl")

  (services (append (fprintd-service-type)
                    base-system-services))
  
  (file-systems (cons* (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "BA76-3723"
                                       'fat32))
                         (type "vfat"))
                       (file-system
                         (mount-point "/")
                         (device (uuid
                                  "db28ba7c-a15d-4c81-8373-99f2f171cac5"
                                  'btrfs))
                         (type "btrfs")
                         (options "ssd,space_cache,clear_cache,compress=zstd"))
                       %base-file-systems)))
