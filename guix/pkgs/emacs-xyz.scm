

(define-module (pkgs emacs-xyz)
  #:use-module (gnu packages)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module ((guix licenses) #:prefix license:))

(define-public emacs-dired-rsync
  (let ((commit "c4f484bff94ed2ddfe0fa45f1e695a1637e1a35a")
        (revision "0"))
    (package
      (name "emacs-dired-rsync")
      (version (git-version "0.0.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/stsquad/dired-rsync.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "0000000000000000000000000000000000000000000000000000"))))
      (build-system emacs-build-system)
      (arguments (list #:emacs emacs-next-pgtk))
      (license license:gpl3+)
      (home-page "https://github.com/stsquad/dired-rsync.git")
      (synopsis "dired-rsync – asynchronous rsync from dired")
      (description "\
This package adds the command dired-rsync which allows the user to copy
marked files in a dired buffer via rsync."))))

(define-public emacs-empv
  (let ((commit "4af3b3624a4714558081d693c91602b9304e8fcb")
        (revision "0"))
    (package
      (name "emacs-empv")
      (version (git-version "0.0.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/isamert/empv.el.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "07xwxqjzah7dg62q0sz6ii8gi1aianqzp51aqql7mg8hyssjzpyj"))))
      (build-system emacs-build-system)
      (arguments (list #:emacs emacs))

      (propagated-inputs
       (list emacs-consult
             emacs-map
             emacs-seq
             emacs-s))
      (license license:gpl3+)
      (home-page "https://github.com/isamert/empv.el.git")
      (synopsis "A multimedia player/manager, YouTube interface")
      (description "An Emacs media player, based on mpv.  More precisely this package provides
somewhat comprehensive interface to mpv with bunch of convenient functionality
like an embedded radio manager, @code{YouTube} interface, local music/video
library manager etc.  Lots of interactive functions are at your disposal.  To
view the most essential ones, type `M-x describe-keymap empv-map`.  It is
advised that you bind this keymap to a key for convenience.  Additionally, empv
has versatile customization options.  For an overview of all customization
options, do `M-x customize-group empv`."))))
