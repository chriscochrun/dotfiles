(define-module (pkgs eww))

(define-public eww
  (package
    (name "eww")
    (version "0.0.1-alpha.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "eww" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0j583vh5kn0k1adsh0q8mdscadlsqximd9scs76sg2n7jy4x19bi"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-egui-wgpu" ,rust-egui-wgpu-0.0.1)
                       ("rust-egui-winit" ,rust-egui-winit-0.0.1))))
    (home-page "https://github.com/LU15W1R7H/eww")
    (synopsis "egui backend (winit + wgpu)")
    (description "egui backend (winit + wgpu)")
    (license license:asl2.0)))
