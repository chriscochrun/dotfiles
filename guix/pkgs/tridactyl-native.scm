
(define-module (pkgs tridactyl-native)
  #:use-module (gnu packages)
  #:use-module (gnu packages nim)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:))

(define-public nim-tempfile
  (let ((commit "96fe74e69838c99641e4f7138c987d5f346c26f1")
        (revision "0"))
    (package
      (name "nim-tempfile")
      (version (git-version "0.1.7" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/OpenSystemsLab/tempfile.nim.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "19w6vv95fm625q6257ibjhvwsdzzjb15qg73k4hcziqhca1f1fm7"))))
      (build-system gnu-build-system)
      (arguments `(#:phases
                   (modify-phases %standard-phases
                     (delete 'configure)
                     (replace 'build
                       (lambda* (#:key outputs #:allow-other-keys)
                         (invoke "nimble" "build"))))))
      (inputs (list nim))
      (license license:expat)
      (home-page "https://github.com/tridactyl/native_messenger.git")
      (synopsis "This small application allows Tridactyl to communicate with your system")
      (description "\
This small application allows Tridactyl to communicate with your system, allowing Tridactyl to:
  - read files on your computer, including RC configuration files
  - write files to your computer
  - launch applications, including opening about:* tabs in Firefox
  - and generally do arbitrary stuff in userspace."))))

(define-public tridactyl-native
  (let ((commit "62f19dba573b924703829847feb1bfee68885514")
        (revision "0"))
    (package
      (name "tridactyl-native")
      (version (git-version "0.3.7" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/tridactyl/native_messenger.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "015x6yx0f4s2digmkay6dbv8hfy5p6rhcy16108xf8swy5qxaq30"))))
      (build-system gnu-build-system)
      (arguments `(#:phases
                   (modify-phases %standard-phases
                     (delete 'configure)
                     (replace 'build
                       (lambda* (#:key outputs #:allow-other-keys)
                         (setenv "XDG_CACHE_HOME" "./cache-home")
                         (setenv "HOME" "./cache-home")
                         (mkdir-p "./cache-home")
                         (invoke "nimble" "build"))))))
      (inputs (list nim nim-tempfile))
      (license license:bsd-2)
      (home-page "https://github.com/tridactyl/native_messenger.git")
      (synopsis "This small application allows Tridactyl to communicate with your system")
      (description "\
This small application allows Tridactyl to communicate with your system, allowing Tridactyl to:
  - read files on your computer, including RC configuration files
  - write files to your computer
  - launch applications, including opening about:* tabs in Firefox
  - and generally do arbitrary stuff in userspace."))))
