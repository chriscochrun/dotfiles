

(define-module (pkgs qt5ct)
  #:use-module (guile)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system qt)
  #:use-module (gnu packages)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (guix gexp))

(define-public qt5ct-kde
  (package
    (inherit qt5ct)
    (version "1.7")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append "mirror://sourceforge/qt5ct/qt5ct-" version ".tar.bz2"))
       (patches '("patches/qt5ct-kde.patch"))
       (sha256
        (base32 "1mxqallxw0b5p4kjfnvv9ndzws54si4a3xlz3gnxyl2cdrm58n7d"))))
    (inputs (modify-inputs
             (package-inputs qt5ct)
             (append qtquickcontrols2-5
                     qtdeclarative-5
                     plasma-framework
                     kconfigwidgets
                     kiconthemes)))))

qt5ct-kde
