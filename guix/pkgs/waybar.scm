(define-module (pkgs waybar))

(define-public waybar
  (package
    (name "waybar")
    (version "0.9.22")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/Alexays/Waybar")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "07h5l7h7wmzqgg7fbp98khrxg2sq2s4ncp4fiiz1yg62r752idy4"))))
    (build-system meson-build-system)
    (arguments
     (list #:configure-flags #~(list "--wrap-mode=nodownload")))
    (inputs (list date
                  fmt
                  gtk-layer-shell
                  gtkmm-3
                  jsoncpp
                  libdbusmenu
                  libevdev
                  libinput-minimal
                  libmpdclient
                  libnl
                  libxml2
                  pipewire
                  playerctl
                  pulseaudio
                  spdlog
                  wayland
                  wireplumber))
    (native-inputs
     (list `(,glib "bin") pkg-config scdoc wayland-protocols))
    (home-page "https://github.com/Alexays/Waybar")
    (synopsis "Wayland bar for Sway and Wlroots based compositors")
    (description "Waybar is a highly customisable Wayland bar for Sway and
Wlroots based compositors.")
    (license license:expat))) ; MIT license

waybar
