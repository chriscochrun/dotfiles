(define-module (pkgs dolphin-xyz)
  #:use-module (gnu packages)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages kde)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (gnu packages kde-systemtools)
  #:use-module (guix build-system qt)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  )

(define-public dolphin-xyz
  (package 
    (inherit dolphin)
    (inputs
     (list baloo
           baloo-widgets
           kactivities
           kbookmarks
           kcmutils
           kcompletion
           kconfig
           kcoreaddons
           kcrash
           kdbusaddons
           ki18n
           kiconthemes
           kinit
           kio
           knewstuff
           knotifications
           kparts
           ktextwidgets
           kuserfeedback
           kwindowsystem
           ffmpegthumbs
           breeze-icons ;; default icon set
           oxygen-icons
           phonon
           qtbase-5
           qtwayland
           wayland
           solid))
    (propogated-inputs '(kfind))))

dolphin-xyz
