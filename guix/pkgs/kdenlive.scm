

(define-module (pkgs kdenlive)
  #:use-module (guix packages)
  #:use-module (guix build-system qt)
  #:use-module (gnu packages)
  #:use-module (gnu packages kde)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (guix gexp))

(define-public kdenlive-fix
  (package
    (inherit kdenlive)
    (name "kdenlive-fix")
    (inputs (modify-inputs
             (package-inputs kdenlive)
             (append kirigami kirigami-addons)))))

kdenlive-fix
