(define-module (pkgs phinger-cursors-theme)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu services)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system copy)
  #:use-module ((guix licenses) #:prefix license:))

(define-public phinger-cursors-theme
  (package
    (name "phinger-cursors-theme")
    (version "1.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/phisch/phinger-cursors/releases/download/v" version "/phinger-cursors-variants.tar.bz2"))
              (sha256
               (base32
                "11ynfizxgfm0dy8i4s0dfws4g9chf12n41hzai8n936wxb3vb3r0"))))
    (build-system copy-build-system)
    (native-inputs (list tar bzip2))
    (arguments
     '(#:install-plan
       '(("../phinger-cursors" "share/icons/phinger-cursors")
         ("../phinger-cursors-light" "share/icons/phinger-cursors-light"))))
    (home-page "https://github.com/phisch/phinger-cursors")
    (synopsis "Most likely the most over engineered cursor theme out there")
    (description
     "Say hello to your new cursor theme. Phinger cursors is most likely the most over engineered cursor theme out there.")
   (license license:cc-by-sa4.0)))

phinger-cursors-theme
