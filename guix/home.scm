;; This is my home environment
;; 
;; 
;; 
;; 

(define-module (home)
  #:use-module (pkgs emacs-xyz)
  #:use-module (pkgs tridactyl-native)
  #:use-module (pkgs phinger-cursors-theme)
  #:use-module (pkgs qt5ct)
  #:use-module (pkgs kdenlive)
  #:use-module (home-services pipewire)
  ;; #:use-module (rde features)
  ;; #:use-module (emacs packages melpa)
  #:use-module (gnu home)
  #:use-module (gnu home services)
  #:use-module (gnu home services desktop)
  #:use-module (gnu home services shells)
  #:use-module (gnu home services xdg)
  #:use-module (gnu home services gnupg)
  #:use-module (gnu home services fontutils)
  #:use-module (gnu home services syncthing)
  #:use-module (gnu packages)
  #:use-module (gnu packages vim)
  #:use-module (gnu packages rust-apps)
  #:use-module (gnu packages video)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages kde)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module ((guix licenses) #:prefix license:))


;; (define snazzy-theme
;;   "A set of the Snazzy colors to be used in colorschemes"
;;   '((base00 . "#282a36")
;;     (base01 . "#34353e")
;;     (base02 . "#43454f")
;;     (base03 . "#78787e")
;;     (base04 . "#a5a5a9")
;;     (base05 . "#e2e4e5")
;;     (base05 . "#eff0eb")
;;     (base06 . "#f1f1f0")
;;     (base06 . "#ff5c57")
;;     (base09 . "#ff9f43")
;;     (base0A . "#f3f99d")
;;     (base0B . "#5af78e")
;;     (base0C . "#9aedfe")
;;     (base0D . "#57c7ff")
;;     (base0E . "#ff6ac1")
;;     (base0F . "#b2643c")))

;; (define mail-config
;;   "a scheme module to setup my email config"
;;   (service-type (name 'mail)
;;                 (extensions
;;                  (list (service-extension
;;                         home-files-service-type
;;                   `((".mbsyncrc" ,(local-file "../.config/.mbsyncrc"))))))))

(define home-packages
  (list ;; Let's start with some things
   ;; CLI apps
   "vim"
   "exa"
   "unzip"
   "rsync"
   "openssh"
   "gcc-toolchain"
   "make"
   "git"
   "curl"
   "vlc"
   "mpv"
   "mpv-mpris"
   "neofetch"
   "qrencode"
   "git:send-email"
   "yt-dlp"
   "playerctl"
   "pavucontrol"
   "alsa-utils"
   "v4l-utils"
   "tmux"
   "direnv"
   "dutree"
   "btop"
   "htop"
   "ripgrep"
   "imagemagick"
   "jq"
   "bat"
   "alacritty"
   "pandoc"
   "fd"
   "bc"
   "sysstat"
   "ffmpeg"
   "ydotool"
   "mediainfo"
   "pulsemixer"
   "pamixer"
   "python-pulsectl"
   "rbw"
   "alsa-utils"
   "wtype"
   "brightnessctl"
   "transmission"
   "blesh"
   "trash-cli"

   ;; Utilities and Libraries
   "libva"
   "libva-utils"
   "libvpx"
   "python"
   "gstreamer"
   "gst-plugins-base"
   "gst-plugins-good"
   "gst-plugins-bad"
   "gst-plugins-ugly"
   "gst-libav"
   "fish-foreign-env"
   "libnotify"
   "ffmpegthumbs"
   "ffmpegthumbnailer"
   "kio"
   "nim"
   "kio-fuse"
   "sshfs"
   ;; "sshfs-fuse"
   "ifuse"
   "libimobiledevice"
   "kio-extras"
   "usbmuxd"
   ;; "nautilus"
   "swaybg"
   "dconf"
   "dconf-editor"
   ;; "tracker"
   ;; "tracker-miners"
   "gvfs"
   ;; "texlive"
   ;; "texlive-pdfx"
   ;; "texlive-latex-pdfx"
   "jmtpfs"
   "libmtp"
   "xfconf"
   "kfind"
   "kiconthemes"
   "gvfs"
   "xfconf"
   "kwallet"
   "kwallet-pam"
   "openjdk"
   "python-vosk"
   "gst-vosk"
   "vosk-api"
   "python-srt"
   "perl-image-exiftool"
   "srt"
   "breeze-icons"
   "distrobox"
   "libreoffice"
   ;; "nemo"
   ;; "tridactyl-native"
   ;; "qtwayland@5.15.8"
   ;; "qtdeclarative@5.15.8"
   "egl-wayland"
   "pinentry"
   "pinentry-qt"
   "pinentry-rofi"
   "flatpak"
   "flatpak-xdg-utils"
   "xdg-utils"
   "xdg-desktop-portal"
   "xdg-desktop-portal-hyprland"
   "xdg-desktop-portal-gtk"
   "aspell"
   "aspell-dict-en"
   "enchant"
   "ncurses"
   "sbcl"

   ;; Theming
   "phinger-cursors-theme" ;; Personal package of cursors
   "papirus-icon-theme"
   "sound-theme-freedesktop"
   "lightly"

   ;; Gui Apps
   "nextcloud-client"
   "dunst"
   "rofi-wayland"
   "waybar"
   "qt5ct"
   "grim"
   "slurp"
   "imv"
   "mpv"
   "mpv-mpris"
   "firefox-wayland"
   "qutebrowser"
   "nyxt"
   "breeze"
   "kdeconnect"
   ;;"plasma"
   "dolphin"
   "darktable"
   "thunar"
   "dconf-editor"
   "ark"
   ;;"kdenlive-fix" ;; Personal fixed kdenlive
   "gimp"
   "distrobox"
   "libreoffice"
   "swaylock-effects"
   "swayidle"
   "steam"
   ;; "tridactyl-native"

   ;; Fonts
   "font-google-noto"
   "font-google-noto-emoji"
   "font-victor-mono"

   ;; Emacs and packages
   "emacs-next-pgtk"
   "emacs-dired-rsync"
   "emacs-all-the-icons"
   "emacs-org-roam"
   "emacs-paredit"
   "emacs-evil-paredit"
   "emacs-doom-modeline"
   "emacs-doom-themes"
   "emacs-org"
   "emacs-elfeed"
   "emacs-elfeed-org"
   "emacs-esh-autosuggest"
   "emacs-use-package"
   "emacs-exec-path-from-shell"
   "emacs-langtool"
   "emacs-avy"
   "emacs-general"
   "emacs-evil"
   "emacs-evil-collection"
   "emacs-no-littering"
   "emacs-evil-escape"
   "emacs-smartparens"
   "emacs-evil-org"
   "emacs-evil-smartparens"
   "emacs-aggressive-indent"
   "emacs-adaptive-wrap"
   "emacs-which-key"
   "emacs-evil-surround"
   "emacs-websocket"
   "emacs-org-modern"
   "emacs-org-web-tools"
   "emacs-org-re-reveal"
   "emacs-org-ql"
   "emacs-org-msg"
   "emacs-nov-el"
   "emacs-calfw"
   "emacs-transmission"
   "emacs-emojify"
   "emacs-pulsar"
   "emacs-ace-link"
   "emacs-vertico"
   "emacs-consult"
   "emacs-consult-eglot"
   "emacs-consult-org-roam"
   "emacs-tempel"
   "emacs-tempel-collection"
   "emacs-marginalia"
   "emacs-embark"
   "emacs-wgrep"
   "emacs-corfu"
   "emacs-cape"
   "emacs-rainbow-delimiters"
   "emacs-orderless"
   "emacs-kind-icon"
   "emacs-projectile"
   "emacs-simple-httpd"
   "emacs-direnv"
   "emacs-sly"
   "emacs-diredfl"
   "emacs-pdf-tools"
   "emacs-vterm"
   "emacs-plz"
   "emacs-empv"
   "emacs-ement"
   "emacs-bongo"
   "emacs-emms"
   "emacs-gcmh"
   "emacs-visual-fill-column"
   "emacs-eat"
   "emacs-mpv"
   "emacs-all-the-icons-dired"
   "emacs-all-the-icons-completion"
   "emacs-org-super-agenda"
   "emacs-toc-org"
   "emacs-ox-reveal"
   "emacs-ox-pandoc"
   "emacs-ox-hugo"
   "emacs-dired-sidebar"
   "emacs-dired-du"
   "emacs-ledger-mode"
   "emacs-toml-mode"
   "emacs-rustic"
   "emacs-lua-mode"
   "emacs-fennel-mode"
   "emacs-web-mode"
   "emacs-qml-mode"
   "emacs-yaml-mode"
   "emacs-cmake-mode"
   "emacs-typescript-mode"
   "emacs-fish-mode"
   "emacs-markdown-mode"
   "emacs-restclient"
   "emacs-ob-restclient"
   "emacs-guix"
   "emacs-nix-mode"
   "emacs-helpful"
   "emacs-mini-echo"
   ;; "emacs-mu4e"
   "isync"
   "mu"))

(home-environment
  (packages (specifications->packages home-packages))

  (services
   (list (service home-bash-service-type
                  (home-bash-configuration
                   (guix-defaults? #t)
                   (aliases '(("grep" . "grep --color=auto")
                              ("gh" . "guix home -L ~/.dotfiles/guix reconfigure ~/.dotfiles/guix/home.scm")
                              ("gs" . "sudo guix system -L /home/chris/.dotfiles/guix reconfigure /home/chris/.dotfiles/guix/$(hostname).scm")
                              ("ytd" . "yt-dlp -o \\\"~/vids/%(title)s.%(ext)s\\\" $1")
                              ("yta" . "yt-dlp -o \\\"~/music/%(title)s.%(ext)s\\\" $1")))
                   (bashrc (list (plain-file "blesh" "source $HOME/.guix-home/profile/share/blesh/ble.sh")
                                 (plain-file "home-manager" "source $HOME/.nix-profile/etc/profile.d/hm-session-vars.sh")
                                 ;;These need to be here so they are at the bottom and therefore after guix-defaults
                                 (plain-file "exal" "alias ls=\"exa -l\"")
                                 (plain-file "exala" "alias la=\"exa -la\"")
                                 (plain-file "less-color" "export LESS=\"--RAW-CONTROL-CHARS\"")
                                 (plain-file "man-colors" "[[ -f ~/.LESS_TERMCAP ]] && . ~/.LESS_TERMCAP")
                                 (plain-file "starship" "source $HOME/.config/starship/init.sh")
                                 (plain-file "direnv" "eval \"$(direnv hook bash)\"")))))
         (simple-service 'extra-env-vars
          		 home-environment-variables-service-type
          		 `(("PATH" . "$PATH:/home/chris/bin:/home/chris/.nix-profile/bin")
                           ("XDG_DATA_DIRS" . "$XDG_DATA_DIRS:$HOME/.local/share/flatpak/exports/share:/var/lib/flatpak/exports/share")
                           ("RTC_USE_PIPEWIRE" . "true")
                           ("GDK_BACKEND" . "wayland,x11")
                           ("WLR_DRM_NO_ATOMIC" . "1")
                           ("WLR_BACKEND" . "vulkan")
                           ("WLR_RENDERER" . "vulkan")
                           ("WLR_NO_HARDWARE_CURSORS" . "1")
                           ("GTK_USE_PORTAL" . "0")
                           ("XCURSOR_THEME" . "phinger-cursors-light")
                           ("EDITOR" . "emacsclient -t -a")
                           ("VISUAL" . "emacsclient -c -a")
                           ("QT_QPA_PLATFORM" . "wayland;xcb")
                           ("QT_AUTO_SCREEN_SCALE_FACTOR" . "1")
                           ("SDL_VIDEODRIVER" . "wayland")
                           ("MOZ_ENABLE_WAYLAND" . "1")
                           ("CLUTTER_BACKEND" . "wayland")
                           ("ELM_ENGINE" . "wayland_egl")
                           ("ECORE_EVAS_ENGINE" . "wayland-egl")
                           ("QT_QPA_PLATFORMTHEME" . "qt5ct")
                           ("LITERAL_VALUE" . ,(literal-string "${abc}"))))
         (service home-fish-service-type
                  (home-fish-configuration
                   (aliases '(("ls" . "exa -l")
                              ("gh" . "guix home -L ~/.dotfiles/guix reconfigure ~/.dotfiles/guix/home.scm")
                              ("gs" . "sudo guix system -L /home/chris/.dotfiles/guix reconfigure /home/chris/.dotfiles/guix/$(hostname).scm")
                              ("ec" . "emacsclient -t")
                              ("ecc" . "emacsclient -c")
                              ("mkdir" . "mkdir -p")
                              ("ytd" . "yt-dlp -o \\\"~/vids/%(title)s.%(ext)s\\\" $1")))
                   (config (list
                            ;; (plain-file "home-manager" "fenv source $HOME/.nix-profile/etc/profile.d/hm-session-vars.sh")
                            (plain-file "direnv" "direnv hook fish | source")
                            (local-file "../.config/fish/config.fish")))))
         (simple-service 'config
                         home-xdg-configuration-files-service-type
                         `(("hypr" ,(local-file "../.config/hypr" #:recursive? #t))
                           ("tridactyl" ,(local-file "../.config/tridactyl" #:recursive? #t))
                           ("eww" ,(local-file "../.config/eww" #:recursive? #t))
                           ("waybar" ,(local-file "../.config/waybar" #:recursive? #t))
                           ("rofi" ,(local-file "../.config/rofi" #:recursive? #t))
                           ("dunst" ,(local-file "../.config/dunst" #:recursive? #t))
                           ("btop" ,(local-file "../.config/bpytop" #:recursive? #t))
                           ("mpv" ,(local-file "../.config/mpv" #:recursive? #t))
                           ("blesh" ,(local-file "../.config/blesh" #:recursive? #t))
                           ("imv" ,(local-file "../.config/imv" #:recursive? #t))
                           ("starship" ,(local-file "../.config/starship" #:recursive? #t))
                           ("starship.toml" ,(local-file "../.config/starship.toml"))
                           ("guix/channels.scm" ,(local-file "channels.scm"))
                           ("home-manager/home.nix" ,(local-file "home.nix"))
                           ("fish/functions" ,(local-file "../.config/fish/functions" #:recursive? #t))
                           ("qt5ct/colors/snazzy.conf" ,(local-file "../.config/qt5ct/snazzy.conf"))
                           ("alacritty/alacritty.yml" ,(local-file "../.config/alacritty/alacritty.yml"))
                           ("qutebrowser" ,(local-file "../.config/qutebrowser" #:recursive? #t))
                           ("direnv/direnvrc" ,(plain-file "direnvrc" "\
use_guixs() {
  eval \"$(guix shell \"$@\" --search-paths)\"
}"))
                           ("gtk-3.0/settings.ini" ,(plain-file "gtk3-settings.ini" "[Settings]
gtk-cursor-theme-name=phinger-cursors-light
gtk-cursor-theme-size=32
gtk-application-prefer-dark-theme=true
gtk-button-images=true
gtk-decoration-layout=icon:minimize,maximize,close
gtk-enable-animations=true
gtk-font-name=VictorMono Nerd Font 11
gtk-icon-theme-name=Papirus-Dark
gtk-menu-images=true
gtk-modules=colorreload-gtk-module:window-decorations-gtk-module:appmenu-gtk-module
gtk-primary-button-warps-slider=true
gtk-shell-shows-menubar=1
gtk-theme-name=Snazzy
gtk-toolbar-style=3
gtk-xft-antialias=1
gtk-xft-hinting=1
gtk-xft-hintstyle=hintfull
gtk-xft-rgba=none
#gtk-xft-dpi=98304
"))
;;                            ("gtk-3.0/gtk.css" ,(plain-file "gtk.css" "

;; "))
;;                            ("gtk-3.0/gtk-dark.css" ,(plain-file "gtk-dark.css"
;;                                                                 "@import url(\"dist/gtk.css\");"))
                           ("gtk-3.0/gtk.css" ,(local-file "../.config/gtk-3.0/gtk.css"))
                           ("gtk-3.0/gtk-dark.css" ,(local-file "../.config/gtk-3.0/gtk-dark.css"))
                           ("gtk-3.0/dist" ,(local-file "../.config/gtk-3.0/dist" #:recursive? #t))
                           ("gtk-3.0/gtk.gresource" ,(local-file "../.config/gtk-3.0/gtk.gresource"))
                           ("gtk-3.0/gtk.gresource.xml" ,(local-file "../.config/gtk-3.0/gtk.gresource.xml"))
                           ("gtk-3.0/assets" ,(local-file "../.config/gtk-3.0/assets" #:recursive? #t))
                           ("gtk-2.0" ,(local-file "../.config/gtk-2.0" #:recursive? #t))))
         (simple-service 'scripts
                         home-files-service-type
                         `(("bin" ,(local-file "../scripts" #:recursive? #t))))
         (simple-service 'home-files
                         home-files-service-type
                         `((".mozilla/firefox/chris.default/chrome" ,(local-file "../.config/firefox/chrome" #:recursive? #t))
                           (".var/app/org.mozilla.firefox/.mozilla/firefox/chris.default/chrome" ,(local-file "../.config/firefox/chrome" #:recursive? #t))
                           (".Xresources" ,(plain-file "Xresources" "Xcursor.theme: phinger-cursors-light
Xcursor.size: 36
"))
                           (".mbsyncrc" ,(local-file "../.config/mbsyncrc"))
                           (".icons/default/index.theme" ,(plain-file "default.theme" "[icon theme]
Inherits=phinger-cursors-light
"))))

         ;; (mail-config)
         (service home-syncthing-service-type)
         (service home-gpg-agent-service-type
                  (home-gpg-agent-configuration
                   (pinentry-program
                    (file-append pinentry-rofi "/bin/pinentry-rofi"))
                   (ssh-support? #t)
                   (default-cache-ttl 28800)
                   (max-cache-ttl 28800)
                   (default-cache-ttl-ssh 28800)
                   (max-cache-ttl-ssh 28800)))

          (simple-service 'additional-fonts-service
                          home-fontconfig-service-type
                          (list "~/.nix-profile/share/fonts"
                                '(alias
                                  (family "monospace")
                                  (prefer
                                   (family "Liberation Mono")))))

         (service home-xdg-user-directories-service-type
                  (home-xdg-user-directories-configuration
                   (videos "$HOME/vids")
                   (pictures "$HOME/pics")
                   (download "$HOME/dls")
                   (documents "$HOME/docs")
                   (music "$HOME/music")
                   (templates "$HOME")
                   (desktop "$HOME")
                   (publicshare "$HOME")))
         (service home-xdg-mime-applications-service-type
                  (home-xdg-mime-applications-configuration
                   (default '(("video/mp4" . "mpv.desktop")
                              ("video/webm" . "mpv.desktop")
                              ("video/x-matroska" . "mpv.destop")
                              ("video/mkv" . "mpv.destop")
                              ("video/quicktime" . "mpv.destop")
                              ("video/mpeg" . "mpv.desktop")
                              ("video/ogg" . "mpv.desktop")
                              ("video/VP9" . "mpv.desktop")
                              ("video/VP8" . "mpv.desktop")
                              ("video/AV1" . "mpv.desktop")
                              ("video/H264" . "mpv.desktop")
                              ("video/H265" . "mpv.desktop")
                              ("video/H266" . "mpv.desktop")
                              ("video/vnd.youtube.yt" . "mpv.desktop")
                              ("inode/directory" . "thunar.desktop")
                              ("application/x-extension-htm" . "firefox.desktop")
                              ("application/x-extension-html" . "firefox.desktop")
                              ("application/x-extension-shtml" . "firefox.desktop")
                              ("application/x-extension-xht" . "firefox.desktop")
                              ("application/x-extension-xhtml" . "firefox.desktop")
                              ("application/xhtml+xml" . "firefox.desktop")
                              ("inode/directory" . "dolphin.desktop")
                              ("image/gif" . "imv-rifle.desktop")
                              ("image/jpeg" . "imv-rifle.desktop")
                              ("image/heif" . "imv-rifle.desktop")
                              ("image/png" . "imv-rifle.desktop")
                              ("image/webp" . "imv-rifle.desktop")
                              ("audio/vorbis" . "mpv-slow.desktop")
                              ("audio/mp3" . "mpv-slow.desktop")
                              ("audio/flac" . "mpv-slow.desktop")
                              ("audio/wav" . "mpv-slow.desktop")
                              ("audio/opus" . "mpv-slow.desktop")))
                   (added '(("video/mp4" . "mpv.desktop")
                            ("video/webm" . "mpv.desktop")
                            ("video/mpeg" . "mpv.desktop")
                            ("video/ogg" . "mpv.desktop")
                            ("video/VP9" . "mpv.desktop")
                            ("video/VP8" . "mpv.desktop")
                            ("video/AV1" . "mpv.desktop")
                            ("video/H264" . "mpv.desktop")
                            ("video/H265" . "mpv.desktop")
                            ("video/H266" . "mpv.desktop")
                            ("video/vnd.youtube.yt" . "mpv.desktop")
                            ("video/x-matroska" . "mpv.destop")
                            ("video/mkv" . "mpv.destop")
                            ("video/quicktime" . "mpv.destop")
                            ("application/x-extension-htm" . "firefox.desktop")
                            ("application/x-extension-html" . "firefox.desktop")
                            ("application/x-extension-shtml" . "firefox.desktop")
                            ("application/x-extension-xht" . "firefox.desktop")
                            ("application/x-extension-xhtml" . "firefox.desktop")
                            ("application/xhtml+xml" . "firefox.desktop")
                            ("image/gif" . "imv-rifle.desktop")
                            ("image/jpeg" . "imv-rifle.desktop")
                            ("image/heif" . "imv-rifle.desktop")
                            ("image/png" . "imv-rifle.desktop")
                            ("image/webp" . "imv-rifle.desktop")
                            ("audio/vorbis" . "mpv-slow.desktop")
                            ("audio/mp3" . "mpv-slow.desktop")
                            ("audio/flac" . "mpv-slow.desktop")
                            ("audio/wav" . "mpv-slow.desktop")
                            ("audio/opus" . "mpv-slow.desktop")))
                   (desktop-entries
                    (list (xdg-desktop-entry
                           (file "mpv-slow")
                           (name "MPV for Audio and Streams")
                           (type 'application)
                           (config
                            '((exec . "mpv --profile=slow %U"))))
                          (xdg-desktop-entry
                           (file "imv-rifle")
                           (name "IMV rifling all files")
                           (type 'application)
                           (config
                            '((exec . "rifle-imv %U"))))))))
         (service home-pipewire-service-type)
         (service home-dbus-service-type))))
