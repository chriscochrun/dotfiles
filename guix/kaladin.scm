;; This file is my desktop OS
;; 
;; Author: Chris Cochrun
;; Email: chris@cochrun.xyz
;; 
;; License: GPLv3
;; 

(define-module (kaladin)
  #:use-module (gnu)
  #:use-module (base)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services dbus)
  #:use-module (gnu system)
  #:use-module (gnu system setuid)
  #:use-module (gnu system nss)
  #:use-module (gnu system shadow)
  #:use-module (gnu packages android)
  #:use-module (rosenthal packages wm)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd))

(operating-system
 (inherit base-operating-system)
 (host-name "kaladin")

 (packages (append (map specification->package
                        '("xf86-video-amdgpu"
                          "radeontop"))
                   base-system-packages))

 (file-systems (cons* (file-system
                       (mount-point "/boot/efi")
                       (device (uuid "35A0-C1F1"
                                     'fat32))
                       (type "vfat"))
                      (file-system
                       (mount-point "/")
                       (device (uuid
                                "9b5a1a62-0de6-4e07-a541-634736980d10"
                                'btrfs))
                       (type "btrfs")
                       (options "ssd,space_cache,clear_cache,compress=zstd"))
                      (file-system
                       (mount-point "/run/media/chris/storage")
                       (device (uuid
                                "4c7d4273-7b72-4aa8-8e1c-e281543d06cb"
                                'btrfs))
                       (type "btrfs")
                       (options "space_cache,clear_cache,compress=zstd"))
                      (file-system
                       (mount-point "/run/media/chris/backup")
                       (device (uuid
                                "4286b9ef-e8ed-49a0-9eec-91b8ee05b2cb"
                                'ext4))
                       (type "ext4"))
                      %base-file-systems)))
