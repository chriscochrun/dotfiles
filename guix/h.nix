{ config, pkgs, ... }:

{
  home.username = "chris";
  home.homeDirectory = "/home/chris";
  home.stateVersion = "23.05";
  programs.home-manager.enable = true;
  home.packages = with pkgs; [
    eww-wayland
    swww
    starship
    # The guix version of dolphin isn't built for wayland and can't find the icon theme
    # dolphin
    # kdenlive
    # glaxnimate
    # ark
    pmbootstrap
    element-desktop-wayland

    intel-media-driver # LIBVA_DRIVER_NAME=iHD
    vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
    libvdpau-va-gl

    #fonts
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    liberation_ttf
    mplus-outline-fonts.githubRelease
    proggyfonts
  ];

  fonts.fontconfig.enable = true;

  programs.firefox = {
    enable = true;
    package = pkgs.firefox-wayland.override {
      cfg = {
        enableTridactylNative = true;
        enablePlasmaBrowserIntegration = true;
      };
    };
    profiles.chris = {
      name = "default";
      path = "chris.default";
      isDefault = true;
    };
  };


  home.file.".mozilla/native-messaging-hosts".source = "/home/chris/.nix-profile/lib/mozilla/native-messaging-hosts";

  accounts.email = {
    maildirBasePath = "mail";
    accounts = {
      personal = {
        address = "chris@cochrun.xyz";
        userName = "chris@cochrun.xyz";
        mbsync.enable = true;
        mu.enable = true;
        flavor = "plain";
        primary = true;
        passwordCommand = "rbw get 'Office 365'";
        realName = "Chris Cochrun";
        signature = {
          text = ''
          Praising God in all things,
          Chris Cochrun
          '';
          delimiter = ''
          ***
          '';
          showSignature = "append";
        };
        imap = {
          host = "mail.cochrun.xyz";
          port = 993;
          tls.enable = true;
        };
        smtp = {
          host = "mail.cochrun.xyz";
          port = 25;
          tls.enable = true;
        };
        mbsync = {
          create = "maildir";
          remove = "both";
          expunge = "both";
        };
        maildir.path = "cochrun";
      };

      work = {
        address = "chris@tfcconnection.org";
        userName = "chris@tfcconnection.org";
        mbsync.enable = true;
        mu.enable = true;
        flavor = "outlook.office365.com";
        passwordCommand = "mailpass";
        realName = "Chris Cochrun";
        imap = {
          host = "outlook.office365.com";
          port = 993;
          tls.enable = true;
        };
        smtp = {
          host = "smtp.office365.com";
          port = 587;
          tls.enable = true;
        };
        mbsync = {
          create = "maildir";
          remove = "both";
          expunge = "both";
          extraConfig.account = {
            AuthMechs = "LOGIN";
          };
        };
        maildir.path = "office";
        signature = {
          text = ''
          Praising God in all things,
          Chris Cochrun
          '';
          delimiter = ''
          ***
          '';
          showSignature = "append";
        };
      };
    };
  };

  programs.mbsync = {
    enable = true;
  };
  programs.msmtp.enable = true;
  services.mbsync.enable = true;
}
